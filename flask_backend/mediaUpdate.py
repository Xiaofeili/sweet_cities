import requests
import pprint
from flask import Flask
import json
import sys
from re import search, split
from models import (
    Company,
    City,
    Role,
    app,
    db,
    generate_picture_url,
    generate_video_url,
)


# Updates model with picture if picture attribute is empty
def update_pictures(table):
    rows = db.session.query(table).filter(table.picture_url == "").all()
    for row in rows:
        prompt = str(row.name)
        row.picture_url = generate_picture_url(prompt)
        db.session.commit()


# Updates model with video if video attribute is empty
def update_videos(table, additional):
    rows = db.session.query(table).filter(table.video_url == "").all()
    for row in rows:
        prompt = str(row.name)
        row.video_url = generate_video_url(prompt, additional)
        db.session.commit()


# Updates all models with media
def run_update():
    print("Updating Company Table")
    update_pictures(Company)
    update_videos(Company, "Company")
    print("Updating City Table")
    update_pictures(City)
    update_videos(City, "City")
    print("Updating Role Table")
    update_pictures(Role)
    update_videos(Role, "Job")
    print("Done")


# Executes Script
if __name__ == "__main__":
    db.create_all()
    run_update()
