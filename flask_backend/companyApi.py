import requests
import pprint
from flask import Flask
import json
import sys
from models import (
    Company,
    City,
    Role,
    app,
    db,
    generate_picture_url,
    generate_video_url,
)

# API security info
api_key = "bq7jr9frh5rcj9a3kcl0"

# Set of valid States
states = {
    "Alaska",
    "Alabama",
    "Arkansas",
    "Arizona",
    "California",
    "Colorado",
    "Connecticut",
    "District of Columbia",
    "Delaware",
    "Florida",
    "Georgia",
    "Guam",
    "Hawaii",
    "Iowa",
    "Idaho",
    "Illinois",
    "Indiana",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Massachusetts",
    "Maryland",
    "Maine",
    "Michigan",
    "Minnesota",
    "Missouri",
    "Northern Mariana Islands",
    "Mississippi",
    "Montana",
    "National",
    "North Carolina",
    "North Dakota",
    "Nebraska",
    "New Hampshire",
    "New Jersey",
    "New Mexico",
    "Nevada",
    "New York",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Rhode Island",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Virginia",
    "Virgin Islands",
    "Vermont",
    "Washington",
    "Wisconsin",
    "West Virginia",
    "Wyoming",
}

# Retrieves all company symbols
def getAllSymbols():
    r = requests.get(
        "https://finnhub.io/api/v1/stock/symbol?exchange=US&token=" + api_key
    )
    return r.json()


# Returns the profile of a company
def getCompany(symbol):
    try:
        re = dict()

        profile_url = (
            "https://finnhub.io/api/v1/stock/profile?symbol="
            + symbol
            + "&token="
            + api_key
        )
        r_profile = requests.get(profile_url)
        re.update(r_profile.json())

        price_url = (
            "https://finnhub.io/api/v1/quote?symbol=" + symbol + "&token=" + api_key
        )
        r_price = requests.get(price_url)
        temp_dict = {"price": r_price.json()["o"]}

        re.update(temp_dict)
    except:
        return None

    return re


# Populates the Company Model within the Database
def updateCompany(symbol):
    profile = getCompany(symbol)
    if profile:

        # Sets attributes for an instance of a Company
        try:
            name = profile["name"]
            company_address = profile["address"]
            company_city = fixName(profile["city"])
            company_state = fixName(profile["state"])
            company_description = profile["description"]
            company_employee_total = profile["employeeTotal"]
            company_market_capitalization = profile["marketCapitalization"]
            company_phone = profile["phone"]
            company_weburl = profile["weburl"]
            company_price = profile["price"]
            company_symbol = profile["ticker"]

            exists = Company.query.filter_by(company_symbol=symbol).scalar() is not None
            if not exists and company_city != "" and company_state in states:
                db.session.add(
                    Company(
                        name=name,
                        company_symbol=company_symbol,
                        company_address=company_address,
                        company_city=company_city,
                        company_state=company_state,
                        company_description=company_description,
                        company_employeeTotal=company_employee_total,
                        company_employeeTotal_readable=get_company_employeeTotal_readable(
                            company_employee_total
                        ),
                        company_marketCapitalization=company_market_capitalization,
                        company_marketCapitalization_readable=get_company_marketCapitalization_readable(
                            company_market_capitalization
                        ),
                        company_phone=company_phone,
                        company_weburl=company_weburl,
                        company_price=company_price,
                        company_price_readable=get_company_price_readable(
                            company_price
                        ),
                        picture_url=generate_picture_url(str(name)),
                        video_url=generate_video_url(name, " company"),
                    )
                )
                db.session.commit()
        except:
            db.session.rollback()


# Converts the employee total into something understandable
def get_company_employeeTotal_readable(index):
    if index == "":
        index = 0
    index = int(index)
    LOW = 200
    MEDIUM = 3000

    if index < LOW:
        return "LOW"
    if index < MEDIUM:
        return "MEDIUM"
    return "HIGH"


# Converts the market capitalization into something understandable
def get_company_marketCapitalization_readable(index):
    if index == "":
        index = 0
    index = int(index)
    LOW = 100
    MEDIUM = 900

    if index < LOW:
        return "LOW"
    if index < MEDIUM:
        return "MEDIUM"
    return "HIGH"


# Converts the company price into something understandable
def get_company_price_readable(index):
    if index == "":
        index = 0
    index = int(index)
    LOW = 20
    MEDIUM = 50

    if index < LOW:
        return "LOW"
    if index < MEDIUM:
        return "MEDIUM"
    return "HIGH"


# Standardizes the State and City name
def fixName(name):
    words = name.split(" ")
    result = str()
    for word in words:
        result += word.capitalize() + " "
    return result[:-1]


# Executes Script
if __name__ == "__main__":
    db.create_all()
    # get the list of available cities from the API
    all_tickers = getAllSymbols()

    # update all company based on their ticker
    for company in all_tickers:
        updateCompany(company["symbol"])
