from flask_sqlalchemy import SQLAlchemy
from models import app, db, City, generate_picture_url, generate_video_url
from yahoo_weather.config.units import Unit
from yahoo_weather.weather import YahooWeather
import requests
import json
import pprint

# API security info
api_key = "zplkyjepwzramj&"
app_id = "90MQpt7i"
yahoo_key = "dj0yJmk9UWpXS1B3ZE85QTdkJmQ9WVdrOU9UQk5VWEIwTjJrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmc3Y9MCZ4PTBj"
yahoo_secret = "9908925372707062a19df7dcefc11f1367f15ac6"


# Retrieves all cities within the United States
def get_all_cities():
    city_dict = dict()
    payload = {"country": "United States"}
    url = "https://www.numbeo.com/api/cities?api_key=" + api_key
    response = requests.get(url, params=payload).json().get("cities")
    for city in response:
        if city["country"] == "United States":
            name = city["city"]
            city_id = city["city_id"]
            city_dict.update({name: city_id})
    return city_dict


# Populates the City Model within the Database
def update_city(name, city_dict, weather):
    url = (
        "https://www.numbeo.com/api/indices?api_key="
        + api_key
        + "&city_id="
        + str(city_dict[name])
    )
    response = requests.get(url).json()

    # Sets attributes for an instance of a City
    try:
        names = response["name"].split(", ")
        city_state = get_state_from_postal(names[1])
        health_care_index = response["health_care_index"]
        health_care_readable = get_health_care_readable(int(health_care_index))
        pollution_index = response["pollution_index"]
        pollution_readable = get_pollution_readable(int(pollution_index))
        climate_index = response["climate_index"]
        climate_index_readable = get_climate_index_readable(int(climate_index))
        city_weather = get_weather(name, weather)
        restaurant_price_index = response["restaurant_price_index"]
        restaurant_price_index_readable = get_restaurant_price_index_readable(
            int(restaurant_price_index)
        )

        exists = db.session.query(City).filter_by(name=name).scalar() is not None
        if not exists:
            db.session.add(
                City(
                    name=names[0],
                    city_state=city_state,
                    city_postal_state=names[1],
                    health_care_index=health_care_index,
                    health_care_readable=health_care_readable,
                    pollution_index=pollution_index,
                    pollution_readable=pollution_readable,
                    climate_index=climate_index,
                    climate_index_readable=climate_index_readable,
                    city_weather=city_weather,
                    restaurant_price_index=restaurant_price_index,
                    restaurant_price_index_readable=restaurant_price_index_readable,
                    picture_url="",
                    video_url="",
                )
            )
            db.session.commit()
    except KeyError:
        pass


# Gets the current weather conditions from yahoo
def get_weather(city, weather):
    weather.get_yahoo_weather_by_city(city, Unit.fahrenheit)
    return str(weather.condition.temperature) + ", " + weather.condition.text


# Converts the health care index into something understandable
def get_health_care_readable(index):
    LOW = 56
    MEDIUM = 78

    if index < LOW:
        return "LOW"
    if index < MEDIUM:
        return "MEDIUM"
    return "HIGH"


# Converts the pollution index into something understandable
def get_pollution_readable(index):
    LOW = 26
    MEDIUM = 51

    if index < LOW:
        return "LOW"
    if index < MEDIUM:
        return "MEDIUM"
    return "HIGH"


# Converts the climate index into something understandable
def get_climate_index_readable(index):
    LOW = 50
    MEDIUM = 70

    if index < LOW:
        return "LOW"
    if index < MEDIUM:
        return "MEDIUM"
    return "HIGH"


# Converts the restaurant index into something understandable
def get_restaurant_price_index_readable(index):
    LOW = 50
    MEDIUM = 70

    if index < LOW:
        return "LOW"
    if index < MEDIUM:
        return "MEDIUM"
    return "HIGH"


# Converts State Postal Code to full name
def get_state_from_postal(postal):
    us_state_abbrev = {
        "AL": "Alabama",
        "AK": "Alaska",
        "AZ": "Arizona",
        "AR": "Arkansas",
        "CA": "California",
        "CO": "Colorado",
        "CT": "Connecticut",
        "DE": "Delaware",
        "FL": "Florida",
        "GA": "Georgia",
        "HI": "Hawaii",
        "ID": "Idaho",
        "IL": "Illinois",
        "IN": "Indiana",
        "IA": "Iowa",
        "KS": "Kansas",
        "KY": "Kentucky",
        "LA": "Louisiana",
        "ME": "Maine",
        "MD": "Maryland",
        "MA": "Massachusetts",
        "MI": "Michigan",
        "MN": "Minnesota",
        "MS": "Mississippi",
        "MO": "Missouri",
        "MT": "Montana",
        "NE": "Nebraska",
        "NV": "Nevada",
        "NH": "New Hampshire",
        "NJ": "New Jersey",
        "NM": "New Mexico",
        "NY": "New York",
        "NC": "North Carolina",
        "ND": "North Dakota",
        "OH": "Ohio",
        "OK": "Oklahoma",
        "OR": "Oregon",
        "PA": "Pennsylvania",
        "RI": "Rhode Island",
        "SC": "South Carolina",
        "SD": "South Dakota",
        "TN": "Tennessee",
        "TX": "Texas",
        "UT": "Utah",
        "VT": "Vermont",
        "VA": "Virginia",
        "WA": "Washington",
        "WV": "West Virginia",
        "WI": "Wisconsin",
        "WY": "Wyoming",
    }
    return us_state_abbrev[postal]


# Executes Script
if __name__ == "__main__":
    db.create_all()
    weather = YahooWeather(APP_ID=app_id, api_key=yahoo_key, api_secret=yahoo_secret)
    city_dict = get_all_cities()
    for name in city_dict:
        update_city(name, city_dict, weather)
