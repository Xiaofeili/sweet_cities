import React, { Component, useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Job from "../pagination/Job";
import PaginationTemplate from "../pagination/PaginationTemplate";
import "../../css/Pagination.css";
import axios from "axios";
import { Redirect } from "react-router-dom";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import { Button } from "mdbreact";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Row from "react-bootstrap/Row";
import Popup from "reactjs-popup";
import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

class JobModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jobIds: [],
      loading: false,
      currentPage: 1,
      numPostPerPage: 9,

      sortBy: "",
      prevSortBy: "",

      orderBy: "",
      prevOrderBy: "",

      filterBy: "",
      prevFilterBy: "",

      searchBy: "",
      prevSearchBy: "",
      searchByValue: "",

      CompareInstances: [],
    };
    this.addDropDownSorting = this.addDropDownSorting.bind(this);
    this.featuredRoles = this.featuredRoles.bind(this);
  }

  // COMPONENTDIDMOUNT -- to make api call to fill the cityIds
  //IF VALID RESPONSE RECEIED (TO GET THE ARRAY OF NAMES OF INSTANCES) from modelType in url
  // THEN SET STATE, OTHERWISE REDIRECT TO 404
  async componentDidMount() {
    const url = "https://api.thesweetcities.com/roles";

    axios
      .get(url)
      .then((response) => {
        this.setState({ jobIds: response.data });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  compareJobs = () => {
    console.log(this.state.CompareInstances)
    if (this.state.CompareInstances.length >= 2) {
      return (
        <>
        <br />
        <h1 className="text-dark my_text_shadow text-center">
          Compare your choices!
        </h1>
        <br />

        <Job
          search={this.state.searchBy}
          Instances={this.state.CompareInstances}
          loading={this.state.loading}
          populateCompareInstances={this.populateCompareInstances}
          fromSearch={true}
        />
        </>
      );
    } else {
      const num = 2-this.state.CompareInstances.length;
      return (
        <div>
          <br />
            <h3 className="text-dark my_text_shadow text-center">
              Select Atleast {num} more Jobs to Compare!
            </h3>
          <br />
        </div>
      );
    }
  }

  populateCompareInstances = (instance) => {
    if (!this.state.CompareInstances.includes(instance)) {
      this.setState({ CompareInstances: [...this.state.CompareInstances, instance] })
      this.notify(instance.name + " added for comparision")
    } else {
      this.notify(instance.name + " is already selected!")
    }
  }

  addCompare = () => {
    return (
      <>
      <Popup trigger={
          <Button 
            style={{ boxShadow: "15px 5px 30px -12px black" }}
          >
          Compare
          </Button>
        }
        closeOnDocumentClick
        modal
        lockScroll
        overlayStyle={{'overflow-y': 'auto'}}
      >
      {close => (
        <div>
          <a className="close" onClick={close}>
          &times;
        </a>
        
        {this.compareJobs()}

        </div>
      )}
        
      </Popup>

      <Button
      style={{ boxShadow: "15px 5px 30px -12px black" }}
      onClick={() => {
        this.setState({CompareInstances: []})
        this.notify("Cleared all choices successfully!")
      }}
      >
        Clear Compare
      </Button>
      </>
    );
  }

  notify = (info) => {
    toast.notify(info, {duration: '2000'});
  }

  paginate = (number, totalInstances) => {
    if (number < 1) {
      number = 1;
    }
    if (number > Math.ceil(totalInstances / this.state.numPostPerPage)) {
      number = Math.ceil(totalInstances / this.state.numPostPerPage);
    }
    this.setState({ currentPage: number });
  };

  addDropDownFiltering = () => {
    const states = [
      "--Filter By State--",
      "Alaska",
      "Alabama",
      "Arkansas",
      "American Samoa",
      "Arizona",
      "California",
      "Colorado",
      "Connecticut",
      "District of Columbia",
      "Delaware",
      "Florida",
      "Georgia",
      "Guam",
      "Hawaii",
      "Iowa",
      "Idaho",
      "Illinois",
      "Indiana",
      "Kansas",
      "Kentucky",
      "Louisiana",
      "Massachusetts",
      "Maryland",
      "Maine",
      "Michigan",
      "Minnesota",
      "Missouri",
      "Mississippi",
      "Montana",
      "North Carolina",
      " North Dakota",
      "Nebraska",
      "New Hampshire",
      "New Jersey",
      "New Mexico",
      "Nevada",
      "New York",
      "Ohio",
      "Oklahoma",
      "Oregon",
      "Pennsylvania",
      "Puerto Rico",
      "Rhode Island",
      "South Carolina",
      "South Dakota",
      "Tennessee",
      "Texas",
      "Utah",
      "Virginia",
      "Virgin Islands",
      "Vermont",
      "Washington",
      "Wisconsin",
      "West Virginia",
      "Wyoming",
    ];

    var title = "--Filter By State--";
    if (this.state.filterBy !== "") {
      title = this.state.filterBy;
    }
    return (
      <>
        <DropdownButton
          as={ButtonGroup}
          key={"state"}
          id={"filter-state"}
          title={title}
          style={{ boxShadow: "15px 5px 30px -12px black" }}
          className="my-2 mx-1 w-auto"
        >
          {states.map((state) => {
            return (
              <Dropdown.Item
                onClick={(e) => {
                  if (e.target.innerHTML == "--Filter By State--") {
                    this.setState({
                      prevFilterBy: this.state.filterBy,
                      filterBy: "",
                    });
                  } else {
                    this.setState({
                      prevFilterBy: this.state.filterBy,
                      filterBy: e.target.innerHTML,
                    });
                  }
                }}
              >
                {state}
              </Dropdown.Item>
            );
          })}
        </DropdownButton>
      </>
    );
  };

  addOrderBy = () => {
    var title = this.state.orderBy == "" ? "--Order By--" : this.state.orderBy;
    const tempOptions = ["--Order By--", "Ascending", "Descending"];
    return (
      <>
        <DropdownButton
          as={ButtonGroup}
          key={"columns"}
          id={"order-job"}
          title={title}
          style={{ boxShadow: "15px 5px 30px -12px black" }}
          className="my-2 mx-1 w-auto"
        >
          {tempOptions.map((tempOption) => {
            return (
              <Dropdown.Item
                onClick={(e) => {
                  if (e.target.innerHTML == "--Order By--") {
                    this.setState({
                      prevOrderBy: this.state.orderBy,
                      orderBy: "",
                    });
                  } else {
                    this.setState({
                      prevOrderBy: this.state.orderBy,
                      orderBy: e.target.innerHTML,
                    });
                  }
                }}
              >
                {tempOption}
              </Dropdown.Item>
            );
          })}
        </DropdownButton>
      </>
    );
  };

  addDropDownSorting() {
    const columns = [
      "--Sort By--",
      "Job Title",
      "State",
      "Median Salary",
      "High Salary",
      "Low Salary",
    ];

    var title = "--Sort By--";
    if (this.state.sortBy !== "") {
      title = this.state.sortBy;
    }
    return (
      <>
        <DropdownButton
          as={ButtonGroup}
          key={"columns"}
          id={"filter-state"}
          title={title}
          style={{ boxShadow: "15px 5px 30px -12px black" }}
          className="my-2 mx-1 w-auto"
        >
          {columns.map((columnName) => {
            return (
              <Dropdown.Item
                onClick={(e) => {
                  if (e.target.innerHTML == "--Sort By--") {
                    this.setState({
                      prevSortBy: this.state.sortBy,
                      sortBy: "",
                    });
                  } else {
                    this.setState({
                      prevSortBy: this.state.sortBy,
                      sortBy: e.target.innerHTML,
                    });
                  }
                }}
              >
                {columnName}
              </Dropdown.Item>
            );
          })}
        </DropdownButton>
      </>
    );
  }

  searchChangeHandler = (event) => {
    this.setState({
      searchByValue: event.target.value,
    });
  };

  searchSubmitHandler = (event) => {
    event.preventDefault();
    this.setState({
      prevSearchBy: this.state.searchBy,
      searchBy: this.state.searchByValue.toLowerCase(),
    });
  };

  addSearchBar = () => {
    return (
      <form
        name="searchform"
        className="form-inline active-cyan-4 my-2"
        onSubmit={this.searchSubmitHandler}
      >
        <div className="active-cyan-4 col">
          <input
            className="form-control"
            type="text"
            placeholder="Search"
            aria-label="Search"
            style={{ boxShadow: "5px 5px 30px -10px black" }}
            onChange={this.searchChangeHandler}
          />
        </div>
        <Button
          type="submit"
          className="btn-rounded btn-primary btn-search"
          style={{ boxShadow: "5px 5px 30px -10px black" }}
        >
          <FontAwesomeIcon icon={faSearch} />
          &thinsp; Search
        </Button>
      </form>
    );
  };

  getSortedBy(url, sortedByParam) {
    if (this.state.sortBy != this.state.prevSortBy || this.state.sortBy != "") {
      if (sortedByParam == "State") {
        sortedByParam = "role_state";
      } else if (sortedByParam == "Median Salary") {
        sortedByParam = "median_salary";
      } else if (sortedByParam == "High Salary") {
        sortedByParam = "high_salary";
      } else if (sortedByParam == "Low Salary") {
        sortedByParam = "Llow_salary";
      } else if (sortedByParam == "Job Title") {
        sortedByParam = "name";
      }

      if (sortedByParam != "") {
        url += "?sorted_by=" + sortedByParam;
      }
    }
    return url;
  }

  getOrderedBy(url, orderedByParam) {
    if (
      this.state.orderBy != this.state.prevOrderBy ||
      this.state.orderBy != ""
    ) {
      if (orderedByParam == "Ascending" || orderedByParam == "") {
        orderedByParam = "asc";
      } else if (orderedByParam == "Descending") {
        orderedByParam = "desc";
      }

      if (this.state.sortBy != "" && orderedByParam != "") {
        url += "&order_by=" + orderedByParam;
      } else if (orderedByParam != "") {
        url += "?order_by=" + orderedByParam;
      }
    }
    return url;
  }

  getFilteredBy(url, filterByParam) {
    if (
      this.state.filterBy != this.state.prevFilterBy ||
      this.state.filterBy != ""
    ) {
      if (
        (this.state.sortBy != "" || this.state.orderBy != "") &&
        this.state.filterBy != ""
      ) {
        url += "&role_state=" + filterByParam;
      } else if (this.state.filterBy != "") {
        url += "?role_state=" + filterByParam;
      }
    }
    return url;
  }

  getSearchBy(url, searchByParam) {
    if (this.state.searchBy != this.state.prevSearchBy || searchByParam != "") {
      if (
        (this.state.sortBy != "" ||
          this.state.orderBy != "" ||
          this.state.filterBy != "") &&
        searchByParam != ""
      ) {
        url += "&searched_by=" + searchByParam;
      } else if (searchByParam != "") {
        url += "?searched_by=" + searchByParam;
      }
    }
    return url;
  }

  featuredRoles(instanceObject) {
    if (
      this.state.sortBy === this.state.prevSortBy &&
      this.state.orderBy === this.state.prevOrderBy &&
      this.state.filterBy === this.state.prevFilterBy &&
      this.state.searchBy === this.state.prevSearchBy
    ) {
      return (
        <Job
          search={this.state.searchBy}
          Instances={instanceObject.Instances}
          loading={this.state.loading}
          populateCompareInstances={this.populateCompareInstances}
        />
      );
    }

    var sortedByParam = this.state.sortBy;
    var orderedByParam = this.state.orderBy;
    var filterByParam = this.state.filterBy;
    var searchByParam = this.state.searchBy;

    var url = "https://api.thesweetcities.com/roles";
    url = this.getSortedBy(url, sortedByParam);
    url = this.getOrderedBy(url, orderedByParam);
    url = this.getFilteredBy(url, filterByParam);
    url = this.getSearchBy(url, searchByParam);

    const ids = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19];
    const [featuredRolesIds, setFeaturedRolesIds] = useState(
      this.state.roleIds
    );
    useEffect(() => {
      async function getRolesIds() {
        axios
          .get(url)
          .then((response) => {
            setFeaturedRolesIds(response.data);
          })
          .catch(() => {
            setFeaturedRolesIds([]);
          });
      }
      getRolesIds();
    }, [
      this.state.sortBy,
      this.state.filterBy,
      this.state.searchBy,
      this.state.orderBy,
    ]);

    if (featuredRolesIds != this.state.RolesIds) {
      this.setState({
        jobIds: featuredRolesIds,
        prevSortBy: this.state.sortBy,
        prevOrderBy: this.state.orderBy,
        prevFilterBy: this.state.filterBy,
        prevSearchBy: this.state.searchBy,
        currentPage: 1,
      });
    }

    // display appropriate paginated instances
    let indexOfLastInstance =
      this.state.currentPage * this.state.numPostPerPage;
    let indexOfFirstInstance =
      this.state.currentPage - this.state.numPostPerPage;
    const currentInstances = this.state.jobIds.slice(
      indexOfFirstInstance,
      indexOfLastInstance
    );

    return (
      <Job
        search={this.state.searchBy}
        Instances={currentInstances}
        loading={this.state.loading}
        populateCompareInstances={this.populateCompareInstances}
      />
    );
  }

  render() {
    let indexOfLastInstance =
      this.state.currentPage * this.state.numPostPerPage;
    let indexOfFirstInstance = indexOfLastInstance - this.state.numPostPerPage;
    let currentInstance = this.state.jobIds.slice(
      indexOfFirstInstance,
      indexOfLastInstance
    );
    return (
      <Container>
        <br />
        <h1 class="text-dark my_text_shadow text-center">Jobs</h1>
        <br />

        <Row className="justify-content-end">
          <div class="col-6">
            <this.addCompare />
          </div>

          <div class="col-6">
            <this.addSearchBar />
          </div>
        </Row>

        <Row className="justify-content-center" style={{ margin: 10 }}>
          <this.addDropDownFiltering style={{ margin: 5 }} />
          
          <this.addDropDownSorting style={{ margin: 5 }} />

          <this.addOrderBy style={{ margin: 5 }} />
          
        </Row>

        <this.featuredRoles Instances={currentInstance} />
        
        <div class="center">
          <PaginationTemplate
            class="pagination"
            totalInstances={this.state.jobIds.length}
            instancesPerPage={this.state.numPostPerPage}
            currentPage={this.state.currentPage}
            paginate={this.paginate}
          />
        </div>
      </Container>
    );
  }
}

export default JobModel;
