import React from "react";
import * as d3 from "d3";
import axios from "axios";
import { Redirect } from "react-router-dom";

import "../../css/CitiesMap.css";

var geoJson = require("./geo.json");

class CitiesMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      citiesInfo: null,
    };
  }

  async componentDidMount() {
    const url = "https://api.thesweetcities.com/cities";
    axios
      .get(url)
      .then((response) => {
        this.setState({ citiesInfo: response.data });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  componentDidUpdate() {
    // Define the div for the tooltip
    var div = d3
      .select("body")
      .append("div")
      .attr("class", "tooltip")
      .text("")
      .style("fill", "#69b3a2");

    const state = [
      "Alabama",
      "Alaska",
      "American Samoa",
      "Arizona",
      "Arkansas",
      "California",
      "Colorado",
      "Connecticut",
      "Delaware",
      "District of Columbia",
      "Federated States of Micronesia",
      "Florida",
      "Georgia",
      "Guam",
      "Hawaii",
      "Idaho",
      "Illinois",
      "Indiana",
      "Iowa",
      "Kansas",
      "Kentucky",
      "Louisiana",
      "Maine",
      "Marshall Islands",
      "Maryland",
      "Massachusetts",
      "Michigan",
      "Minnesota",
      "Mississippi",
      "Missouri",
      "Montana",
      "Nebraska",
      "Nevada",
      "New Hampshire",
      "New Jersey",
      "New Mexico",
      "New York",
      "North Carolina",
      "North Dakota",
      "Northern Mariana Islands",
      "Ohio",
      "Oklahoma",
      "Oregon",
      "Palau",
      "Pennsylvania",
      "Puerto Rico",
      "Rhode Island",
      "South Carolina",
      "South Dakota",
      "Tennessee",
      "Texas",
      "Utah",
      "Vermont",
      "Virgin Island",
      "Virginia",
      "Washington",
      "West Virginia",
      "Wisconsin",
      "Wyoming",
    ];

    var data = [];
    for (let s in state) {
      var st = state[s];
      data.push({ state: st, avg: 0, tag: "Low", value: 0, count: 0 });
    }

    for (let key in this.state.citiesInfo) {
      var city = this.state.citiesInfo[key];
      var city_state = city["city_state"];
      var index_state = state.indexOf(city_state);
      data[index_state].count += 1;
      data[index_state].value += Number(city["restaurant_price_index"]);
    }

    for (let key in data) {
      if (data[key].value == 0) {
        data[key].avg = 0;
      } else {
        data[key].avg = data[key].value / data[key].count;
      }
    }

    

    data.sort(function (a, b) {
      return a.avg - b.avg;
    });

    for (let i = 30; i < 50; i++) {
      data[i].tag = "Medium";
    }

    for (let i = 50; i < 59; i++) {
      data[i].tag = "High";
    }

    data.sort(function (a, b) {
      // return a.state < b.state;
      if (a.state < b.state) {
        return -1;
      } else if (a.state > b.state) {
        return 1;
      } else {
        return 0;
      }
    });

    var width = 1300;
    var height = 600;
    
    var projection = d3
      .geoAlbersUsa()
      .translate([width / 2, height / 2 + 30]) // translate to center of screen
      .scale([1000]);
      

    var path = d3.geoPath().projection(projection);

    var legendText = ["High", "Medium", "Low"];

    var svg = d3.select("svg").attr("width", width).attr("height", height);

    svg
      .append("text")
      .attr("x", 330)
      .attr("y", -50)
      .attr("dy", "3.5em")
      .attr("text-anchor", "start")
      .style("font-size", "28px")
      .style("font-weight", "bold")
      .text("Displays the average level of cost of living in each state");

    
    for (var i = 0; i < data.length; i++) {
      var dataState = data[i].state;
      var dataValue = data[i].tag;
      var dataAvg = data[i].avg;

      for (var j = 0; j < geoJson.features.length; j++) {
        var jsonState = geoJson.features[j].properties.name;

        if (dataState == jsonState) {
          geoJson.features[j].properties.tag = dataValue;
          geoJson.features[j].properties.avg = Number(dataAvg).toFixed(2);
          break;
        }
      }
    }

    for (var j = 0; j < geoJson.features.length; j++) {
      if (geoJson.features[j].properties.tag == null) {
        geoJson.features[j].properties.tag = "Low";
        geoJson.features[j].properties.avg = 0;
      }
    }

    var color = d3
      .scaleOrdinal()
      .domain(legendText)
      .range(["#021B79", "#0575E6", "#ADD8E6"]);

    svg
      .selectAll("path")
      .data(geoJson.features)
      .enter()
      .append("path")
      .attr("d", path)
      .style("stroke", "#fff")
      .style("stroke-width", "1")
      .style("fill", function (d) {
        var value = d.properties.tag;
        if (value) {
          return color(value);
        } else {
          return "rgb(213,222,217)";
        }
      })
      .on("mouseover", function (d) {
        div.transition().duration(200).style("opacity", 0.9);
        var duration = 300;
        data.forEach(function (d, i) {
          svg
            .transition()
            .duration(duration)
            .delay(i * duration)
            .attr("r", d.avg);
        });
        div
          .html(d.properties.name + ": <br>" + d.properties.avg)
          .style("left", d3.event.pageX + "px")
          .style("top", d3.event.pageY - 28 + "px");
      })
      .on("mouseout", function (d) {
        d3.select(this)
          .transition()
          .duration(250)
          .attr(
            "fill",
            "rgb(0," + (123 - d.value * 3) + ", " + (255 - d.value * 4) + ")"
          );
        div.transition().duration(200).style("opacity", 0);
      });;

    var legend = svg
      .selectAll(".legend")
      .attr("class", "legend")
      .attr("width", 250)
      .attr("height", 150)
      .data(color.domain().slice())
      .enter()
      .append("g")
      .attr("transform", function (d, i) {
        return "translate(1000," + (i * 20 + 450) + ")";
      });

    legend
      .append("rect")
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

    legend
      .append("text")
      .data(legendText)
      .attr("x", 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .text(function (d) {
        return d;
      });
  }

  render() {
    return <svg></svg>;
  }
}

export default CitiesMap;
