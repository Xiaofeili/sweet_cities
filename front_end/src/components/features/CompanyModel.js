import React, { Component, useState, useEffect } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";

import Company from "../pagination/Company";
import PaginationTemplate from "../pagination/PaginationTemplate";
import "../../css/Pagination.css";
import "../../css/Search.css";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import ButtonGroup from "react-bootstrap/ButtonGroup";
import { Button } from "mdbreact";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";

import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Popup from "reactjs-popup";
import toast from 'toasted-notes' 
import 'toasted-notes/src/styles.css';

class CompanyModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      companyIds: [],
      loading: false,
      currentPage: 1,
      numPostPerPage: 9,

      sortBy: "",
      prevSortBy: "",

      orderBy: "",
      prevOrderBy: "",

      filterBy: "",
      prevFilterBy: "",
      filterAttrBy: "",
      prevFilterAttrBy: "",
      filterValBy: "",
      prevFilterValBy: "",

      searchBy: "",
      prevSearchBy: "",
      searchByValue: "",

      CompareInstances: [],
    };
    this.addDropDownSorting = this.addDropDownSorting.bind(this);
    this.featuredCompanies = this.featuredCompanies.bind(this);
  }

  // COMPONENTDIDMOUNT -- to make api call to fill the instanceList
  //IF VALID RESPONSE RECEIED (TO GET THE ARRAY OF NAMES OF INSTANCES) from modelType in url
  // THEN SET STATE, OTHERWISE REDIRECT TO 404
  async componentDidMount() {
    const url = "https://api.thesweetcities.com/companies";

    axios
      .get(url)
      .then((response) => {
        this.setState({ companyIds: response.data });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  compareCompanies = () => {
    console.log(this.state.CompareInstances)
    if (this.state.CompareInstances.length >= 2) {
      return (
        <>
        <br />
        <h1 className="text-dark my_text_shadow text-center">
          Compare your choices!
        </h1>
        <br />

        <Company
          search={this.state.searchBy}
          Instances={this.state.CompareInstances}
          loading={this.state.loading}
          populateCompareInstances={this.populateCompareInstances}
          fromSearch={true}
        />
        </>
      );
    } else {
      const num = 2-this.state.CompareInstances.length;
      return (
        <div>
          <br />
            <h3 className="text-dark my_text_shadow text-center">
              Select Atleast {num} more Companies to Compare!
            </h3>
          <br />
        </div>
      );
    }
  }

  populateCompareInstances = (instance) => {
    if (!this.state.CompareInstances.includes(instance)) {
      this.setState({ CompareInstances: [...this.state.CompareInstances, instance] })
      this.notify(instance.name + " added for comparision")
    } else {
      this.notify(instance.name + " is already selected!")
    }
  }

  addCompare = () => {
    return (
      <>
      <Popup trigger={
          <Button 
            style={{ boxShadow: "15px 5px 30px -12px black" }}
          >
          Compare
          </Button>
        }
        closeOnDocumentClick
        modal
        lockScroll
        overlayStyle={{'overflow-y': 'auto'}}
      >
      {close => (
        <div>
          <a className="close" onClick={close}>
          &times;
        </a>
        
        {this.compareCompanies()}

        </div>
      )}
        
      </Popup>

      <Button
      style={{ boxShadow: "15px 5px 30px -12px black" }}
      onClick={() => {
        this.setState({CompareInstances: []})
        this.notify("Cleared all choices successfully!")
      }}
      >
        Clear Compare
      </Button>
      </>
    );
  }

  notify = (info) => {
    toast.notify(info, {duration: '2000'});
  }

  paginate = (number, totalInstances) => {
    if (number < 1) {
      number = 1;
    }
    if (number > Math.ceil(totalInstances / this.state.numPostPerPage)) {
      number = Math.ceil(totalInstances / this.state.numPostPerPage);
    }
    this.setState({ currentPage: number });
  };

  addDropDownFilteringValues = () => {
    const columns = ["--Value for Index--", "HIGH", "MEDIUM", "LOW"];

    var title = "--Value for Index--";
    if (this.state.filterValBy !== "") {
      title = this.state.filterValBy;
    }

    return (
      <>
        <DropdownButton
          as={ButtonGroup}
          key={"columns"}
          id={"filter-value"}
          title={title}
          style={{ boxShadow: "15px 5px 30px -12px black" }}
          className="my-2 mx-1 w-auto"
        >
          {columns.map((columnName) => {
            return (
              <Dropdown.Item
                onClick={(e) => {
                  if (e.target.innerHTML == "--Value for Index--") {
                    this.setState({
                      prevFilterValBy: this.state.filterValBy,
                      filterValBy: "",
                    });
                  } else {
                    this.setState({
                      prevFilterValBy: this.state.filterValBy,
                      filterValBy: e.target.innerHTML,
                    });
                  }
                }}
              >
                {columnName}
              </Dropdown.Item>
            );
          })}
        </DropdownButton>
      </>
    );
  };

  addDropDownFilteringAttributes = () => {
    const columns = [
      "--Filter By Index--",
      "Stock Price",
      "Market Cap",
      "Total Employees",
    ];

    var title = "--Filter By Index--";
    if (this.state.filterAttrBy !== "") {
      title = this.state.filterAttrBy;
    }

    return (
      <>
        <DropdownButton
          as={ButtonGroup}
          key={"columns"}
          id={"filter-attribute"}
          title={title}
          style={{ boxShadow: "15px 5px 30px -12px black" }}
          className="my-2 mx-1 w-auto"
        >
          {columns.map((columnName) => {
            return (
              <Dropdown.Item
                onClick={(e) => {
                  if (e.target.innerHTML == "--Filter By Index--") {
                    this.setState({
                      prevFilterAttrBy: this.state.filterAttrBy,
                      filterAttrBy: "",
                    });
                  } else {
                    this.setState({
                      prevFilterAttrBy: this.state.filterAttrBy,
                      filterAttrBy: e.target.innerHTML,
                    });
                  }
                }}
              >
                {columnName}
              </Dropdown.Item>
            );
          })}
        </DropdownButton>
      </>
    );
  };

  searchChangeHandler = (event) => {
    this.setState({
      searchByValue: event.target.value,
    });
  };

  searchSubmitHandler = (event) => {
    event.preventDefault();
    this.setState({
      prevSearchBy: this.state.searchBy,
      searchBy: this.state.searchByValue.toLowerCase(),
    });
  };

  addSearchBar = () => {
    return (
      <form
        name="searchform"
        className="form-inline active-cyan-4 my-2"
        onSubmit={this.searchSubmitHandler}
      >
        <div className="active-cyan-4 col">
          <input
            className="form-control"
            type="text"
            placeholder="Search"
            aria-label="Search"
            style={{ boxShadow: "5px 5px 30px -10px black" }}
            onChange={this.searchChangeHandler}
          />
        </div>
        <Button
          type="submit"
          className="btn-rounded btn-primary btn-search"
          style={{ boxShadow: "5px 5px 30px -10px black" }}
        >
          <FontAwesomeIcon icon={faSearch} />
          &thinsp; Search
        </Button>
      </form>
    );
  };

  addDropDownFiltering = () => {
    const states = [
      "--Filter By State--",
      "Alaska",
      "Alabama",
      "Arkansas",
      "American Samoa",
      "Arizona",
      "California",
      "Colorado",
      "Connecticut",
      "District of Columbia",
      "Delaware",
      "Florida",
      "Georgia",
      "Guam",
      "Hawaii",
      "Iowa",
      "Idaho",
      "Illinois",
      "Indiana",
      "Kansas",
      "Kentucky",
      "Louisiana",
      "Massachusetts",
      "Maryland",
      "Maine",
      "Michigan",
      "Minnesota",
      "Missouri",
      "Mississippi",
      "Montana",
      "North Carolina",
      " North Dakota",
      "Nebraska",
      "New Hampshire",
      "New Jersey",
      "New Mexico",
      "Nevada",
      "New York",
      "Ohio",
      "Oklahoma",
      "Oregon",
      "Pennsylvania",
      "Puerto Rico",
      "Rhode Island",
      "South Carolina",
      "South Dakota",
      "Tennessee",
      "Texas",
      "Utah",
      "Virginia",
      "Virgin Islands",
      "Vermont",
      "Washington",
      "Wisconsin",
      "West Virginia",
      "Wyoming",
    ];

    var title = "--Filter By State--";
    if (this.state.filterBy !== "") {
      title = this.state.filterBy;
    }
    return (
      <>
        <DropdownButton
          as={ButtonGroup}
          key={"state"}
          id={"filter-state-company"}
          title={title}
          style={{ boxShadow: "15px 5px 30px -12px black" }}
          className="my-2 mx-1 w-auto"
        >
          {states.map((state) => {
            return (
              <Dropdown.Item
                onClick={(e) => {
                  if (e.target.innerHTML == "--Filter By State--") {
                    this.setState({
                      prevFilterBy: this.state.filterBy,
                      filterBy: "",
                    });
                  } else {
                    this.setState({
                      prevFilterBy: this.state.filterBy,
                      filterBy: e.target.innerHTML,
                    });
                  }
                }}
              >
                {state}
              </Dropdown.Item>
            );
          })}
        </DropdownButton>
      </>
    );
  };

  addOrderBy = () => {
    var title = this.state.orderBy == "" ? "--Order By--" : this.state.orderBy;
    const tempOptions = ["--Order By--", "Ascending", "Descending"];
    return (
      <>
        <DropdownButton
          as={ButtonGroup}
          key={"columns"}
          id={"order-company"}
          title={title}
          style={{ boxShadow: "15px 5px 30px -12px black" }}
          className="my-2 mx-1 w-25"
        >
          {tempOptions.map((tempOption) => {
            return (
              <Dropdown.Item
                onClick={(e) => {
                  if (e.target.innerHTML == "--Order By--") {
                    this.setState({
                      prevOrderBy: this.state.orderBy,
                      orderBy: "",
                    });
                  } else {
                    this.setState({
                      prevOrderBy: this.state.orderBy,
                      orderBy: e.target.innerHTML,
                    });
                  }
                }}
              >
                {tempOption}
              </Dropdown.Item>
            );
          })}
        </DropdownButton>
      </>
    );
  };

  addDropDownSorting() {
    const columns = [
      "--Sort By--",
      "Company Name",
      "State",
      "Stock Price",
      "Market Cap",
      "Total Employees",
    ];

    var title = "--Sort By--";
    if (this.state.sortBy !== "") {
      title = this.state.sortBy;
    }
    return (
      <>
        <DropdownButton
          as={ButtonGroup}
          key={"columns"}
          id={"sort-company"}
          title={title}
          style={{ boxShadow: "15px 5px 30px -12px black" }}
          className="my-2 mx-1 w-auto"
        >
          {columns.map((columnName) => {
            return (
              <Dropdown.Item
                onClick={(e) => {
                  if (e.target.innerHTML == "--Sort By--") {
                    this.setState({
                      prevSortBy: this.state.sortBy,
                      sortBy: "",
                    });
                  } else {
                    this.setState({
                      prevSortBy: this.state.sortBy,
                      sortBy: e.target.innerHTML,
                    });
                  }
                }}
              >
                {columnName}
              </Dropdown.Item>
            );
          })}
        </DropdownButton>
      </>
    );
  }

  getSortedBy(url, sortedByParam) {
    if (this.state.sortBy != this.state.prevSortBy || this.state.sortBy != "") {
      if (sortedByParam == "Company Name") {
        sortedByParam = "name";
      } else if (sortedByParam == "City") {
        sortedByParam = "company_city";
      } else if (sortedByParam == "State") {
        sortedByParam = "company_state";
      } else if (sortedByParam == "Stock Price") {
        sortedByParam = "company_price";
      } else if (sortedByParam == "Market Cap") {
        sortedByParam = "company_marketCapitalization";
      } else if (sortedByParam == "Total Employees") {
        sortedByParam = "company_employeeTotal";
      }

      if (sortedByParam != "") {
        url += "?sorted_by=" + sortedByParam;
      }
    }
    return url;
  }

  getOrderedBy(url, orderedByParam) {
    if (
      this.state.orderBy != this.state.prevOrderBy ||
      this.state.orderBy != ""
    ) {
      if (orderedByParam == "Ascending" || orderedByParam == "") {
        orderedByParam = "asc";
      } else if (orderedByParam == "Descending") {
        orderedByParam = "desc";
      }

      if (this.state.sortBy != "" && orderedByParam != "") {
        url += "&order_by=" + orderedByParam;
      } else if (orderedByParam != "") {
        url += "?order_by=" + orderedByParam;
      }
    }
    return url;
  }

  getFilteredBy(url, filterByParam) {
    if (
      this.state.filterBy != this.state.prevFilterBy ||
      this.state.filterBy != ""
    ) {
      if (
        (this.state.sortBy != "" || this.state.orderBy != "") &&
        this.state.filterBy != ""
      ) {
        url += "&company_state=" + filterByParam;
      } else if (this.state.filterBy != "") {
        url += "?company_state=" + filterByParam;
      }
    }
    return url;
  }

  getSearchBy(url, searchByParam) {
    if (this.state.searchBy != this.state.prevSearchBy || searchByParam != "") {
      if (
        (this.state.sortBy != "" ||
          this.state.orderBy != "" ||
          this.state.filterBy != "") &&
        searchByParam != ""
      ) {
        url += "&searched_by=" + searchByParam;
      } else if (searchByParam != "") {
        url += "?searched_by=" + searchByParam;
      }
    }
    return url;
  }

  getFilteredAttrBy(url, filterAttrByParam) {
    if (
      this.state.filterAttrBy != this.state.prevFilterAttrBy ||
      filterAttrByParam != ""
    ) {
      if (this.state.filterValBy != "") {
        if (filterAttrByParam == "Stock Price") {
          filterAttrByParam = "company_price_readable";
        } else if (filterAttrByParam == "Market Cap") {
          filterAttrByParam = "company_marketCapitalization_readable";
        } else if (filterAttrByParam == "Total Employees") {
          filterAttrByParam = "company_employeeTotal_readable";
        }

        if (
          (this.state.sortBy != "" ||
            this.state.orderBy != "" ||
            this.state.filterBy != "" ||
            this.state.searchBy != "") &&
          filterAttrByParam != ""
        ) {
          url += "&" + filterAttrByParam + "=" + this.state.filterValBy;
        } else if (filterAttrByParam != "") {
          url += "?" + filterAttrByParam + "=" + this.state.filterValBy;
        }
      }
    }
    return url;
  }

  featuredCompanies(instanceObject) {
    if (
      this.state.sortBy === this.state.prevSortBy &&
      this.state.orderBy === this.state.prevOrderBy &&
      this.state.filterBy === this.state.prevFilterBy &&
      this.state.filterAttrBy === this.state.prevFilterAttrBy &&
      this.state.filterValBy === this.state.prevFilterValBy &&
      this.state.searchBy === this.state.prevSearchBy
    ) {
      return (
        <Company
          search={this.state.searchBy}
          Instances={instanceObject.Instances}
          loading={this.state.loading}
          populateCompareInstances={this.populateCompareInstances}
        />
      );
    }

    var sortedByParam = this.state.sortBy;
    var orderedByParam = this.state.orderBy;
    var filterByParam = this.state.filterBy;
    var filterAttrByParam = this.state.filterAttrBy;
    var searchByParam = this.state.searchBy;

    var url = "https://api.thesweetcities.com/companies";
    url = this.getSortedBy(url, sortedByParam);
    url = this.getOrderedBy(url, orderedByParam);
    url = this.getFilteredBy(url, filterByParam);
    url = this.getSearchBy(url, searchByParam);
    url = this.getFilteredAttrBy(url, filterAttrByParam);

    var curPage = false;
    if (
      (this.state.filterAttrBy != this.state.prevFilterAttrBy &&
        this.state.filterValBy == "") ||
      (this.state.filterValBy != this.state.prevFilterValBy &&
        this.state.filterAttrBy == "")
    ) {
      curPage = true;
    }

    const ids = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19];
    const [featuredCompanyIds, setFeaturedCompanyIds] = useState(
      this.state.companyIds
    );

    useEffect(() => {
      async function getCompanyIds() {
        axios
          .get(url)
          .then((response) => {
            setFeaturedCompanyIds(response.data);
          })
          .catch(() => {
            setFeaturedCompanyIds([]);
          });
      }
      getCompanyIds();
    }, [
      this.state.sortBy,
      this.state.filterBy,
      this.state.filterAttrBy,
      this.state.filterValBy,
      this.state.searchBy,
      this.state.orderBy,
    ]);

    var tempCurrPage = 1;
    if (curPage) {
      tempCurrPage = this.state.currentPage;
    }

    if (featuredCompanyIds != this.state.companyIds) {
      this.setState({
        companyIds: featuredCompanyIds,
        prevSortBy: this.state.sortBy,
        prevOrderBy: this.state.orderBy,
        prevFilterBy: this.state.filterBy,
        prevFilterAttrBy: this.state.filterAttrBy,
        prevFilterValBy: this.state.filterValBy,
        prevSearchBy: this.state.searchBy,
        currentPage: tempCurrPage,
      });
    }

    // display appropriate paginated instances
    let indexOfLastInstance =
      this.state.currentPage * this.state.numPostPerPage;
    let indexOfFirstInstance =
      this.state.currentPage - this.state.numPostPerPage;
    const currentInstances = this.state.companyIds.slice(
      indexOfFirstInstance,
      indexOfLastInstance
    );

    return (
      <Company
        search={this.state.searchBy}
        Instances={currentInstances}
        loading={this.state.loading}
        populateCompareInstances={this.populateCompareInstances}
      />
    );
  }

  render() {
    let indexOfLastInstance =
      this.state.currentPage * this.state.numPostPerPage;
    let indexOfFirstInstance = indexOfLastInstance - this.state.numPostPerPage;
    let currentInstance = this.state.companyIds.slice(
      indexOfFirstInstance,
      indexOfLastInstance
    );
    
    return (
      <Container>
        <br />
        <h1 class="text-dark my_text_shadow text-center">Companies</h1>
        <br />

        <Row className="justify-content-end">
          <div class="col-6">
            <this.addCompare />
          </div>

          <div class="col-6">
            <this.addSearchBar />
          </div>
        </Row>

        <Row className="justify-content-center" style={{ margin: 10 }}>
          <this.addDropDownFiltering style={{ margin: 5 }} />

          <this.addDropDownFilteringAttributes style={{ margin: 5 }} />

          <this.addDropDownFilteringValues style={{ margin: 5 }} />

          <Col lg={5} className="text-center">
            <this.addDropDownSorting style={{ margin: 5 }} />

            <this.addOrderBy style={{ margin: 5 }} />
          </Col>
        </Row>

        <this.featuredCompanies Instances={currentInstance} />

        <div class="center">
          <PaginationTemplate
            class="pagination"
            totalInstances={this.state.companyIds.length}
            instancesPerPage={this.state.numPostPerPage}
            currentPage={this.state.currentPage}
            paginate={this.paginate}
          />
        </div>
      </Container>
    );
  }
}

export default CompanyModel;
