import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import axios from "axios";
import Job from "../pagination/Job";
import City from "../pagination/City";
import Company from "../pagination/Company";
import PaginationTemplate from "../pagination/PaginationTemplate";
import "../../css/Pagination.css";
import { Tab, Tabs, TabList } from "react-tabs";
import "react-tabs/style/react-tabs.css";

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currUrlBase: "",
      prevUrlBase: "",
      loading: true,

      currentPage: 1,
      numPostPerPage: 9,
      tabIndex: 0,
      tabTitles: [],

      cityIds: [],
      companyIds: [],
      jobIds: [],
      cityInfo: "",
      companyInfo: "",
      jobInfo: "",
    };
    this.modelsBar = this.modelsBar.bind(this);
    this.renderInstances = this.renderInstances.bind(this);
  }

  async componentDidMount() {
    console.log(this.props.match.params.searchQuerry);

    var url =
      "https://api.thesweetcities.com/cities?" +
      "searched_by=" +
      this.props.match.params.searchQuerry;
    axios
      .get(url)
      .then((response) => {
        if (response.data[0]) {
          this.setState({
            cityIds: response.data,
            prevUrlBase: "cities",
            currUrlBase: "cities",
            tabTitles: ["cities", "companies", "roles"],
          });
        } else {
          this.setState({
            cityInfo: "Nothing to show in cities",
            prevUrlBase: "cities",
            currUrlBase: "cities",
            tabTitles: ["cities", "companies", "roles"],
          });
        }
      })
      .catch(() => {
        console.log("BAD API RESPONSE");
      });

    url =
      "https://api.thesweetcities.com/companies?" +
      "searched_by=" +
      this.props.match.params.searchQuerry;
    axios
      .get(url)
      .then((response) => {
        if (response.data[0]) {
          this.setState({
            companyIds: response.data,
          });
        } else {
          this.setState({
            companyInfo: "Nothing to show in Companies",
          });
        }
      })
      .catch(() => {
        console.log("BAD API RESPONSE");
      });

    url =
      "https://api.thesweetcities.com/roles?" +
      "searched_by=" +
      this.props.match.params.searchQuerry;
    axios
      .get(url)
      .then((response) => {
        if (response.data[0]) {
          this.setState({
            jobIds: response.data,
            loading: false,
          });
        } else {
          this.setState({
            jobInfo: "Nothing to show in Jobs",
            loading: false,
          });
        }
      })
      .catch(() => {
        console.log("BAD API RESPONSE");
      });
  }

  paginate = (number, totalInstances) => {
    if (number < 1) {
      number = 1;
    }
    if (number > Math.ceil(totalInstances / this.state.numPostPerPage)) {
      number = Math.ceil(totalInstances / this.state.numPostPerPage);
    }
    this.setState({ currentPage: number });
  };

  renderInstances() {
    if (this.state.currUrlBase == "cities") {
      console.log("about to return city instances");
      const instances = this.calculatePagination(
        this.state.cityIds,
        this.state.cityInfo
      );
      console.log(this.state.cityIds);
      if (!instances[0]) {
        return (
          <h1 class="text-dark my_text_shadow text-center">
            {this.state.cityInfo}
          </h1>
        );
      }

      return (
        <>
          <City
            Instances={instances}
            loading={this.state.loading}
            search={this.props.match.params.searchQuerry}
            fromSearch={true}
          />
          <div class="center">
            <PaginationTemplate
              className="pagination"
              totalInstances={this.state.cityIds.length}
              instancesPerPage={this.state.numPostPerPage}
              currentPage={this.state.currentPage}
              paginate={this.paginate}
            />
          </div>
        </>
      );
    } else if (this.state.currUrlBase == "companies") {
      console.log("about to return company instances");
      const instances = this.calculatePagination(
        this.state.companyIds,
        this.state.companyInfo
      );

      if (!instances[0]) {
        return (
          <h1 class="text-dark my_text_shadow text-center">
            {this.state.companyInfo}
          </h1>
        );
      }

      return (
        <>
          <Company
            Instances={instances}
            loading={this.state.loading}
            search={this.props.match.params.searchQuerry}
            fromSearch={true}
          />
          <div class="center">
            <PaginationTemplate
              className="pagination"
              totalInstances={this.state.companyIds.length}
              instancesPerPage={this.state.numPostPerPage}
              currentPage={this.state.currentPage}
              paginate={this.paginate}
            />
          </div>
        </>
      );
    } else if (this.state.currUrlBase == "roles") {
      console.log("about to return job instances");
      const instances = this.calculatePagination(
        this.state.jobIds,
        this.state.jobInfo
      );

      if (!instances[0]) {
        return (
          <h1 class="text-dark my_text_shadow text-center">
            {this.state.jobInfo}
          </h1>
        );
      }

      return (
        <>
          <Job
            Instances={instances}
            loading={this.state.loading}
            search={this.props.match.params.searchQuerry}
            fromSearch={true}
          />
          <div class="center">
            <PaginationTemplate
              className="pagination"
              totalInstances={this.state.jobIds.length}
              instancesPerPage={this.state.numPostPerPage}
              currentPage={this.state.currentPage}
              paginate={this.paginate}
            />
          </div>
        </>
      );
    }

    return (
      <h1 class="text-dark my_text_shadow text-center">{this.state.info}</h1>
    );
  }

  modelsBar() {
    return (
      <Tabs
        selectedIndex={this.state.tabIndex}
        onSelect={(temptabIndex) => {
          if (temptabIndex != this.state.tabIndex) {
            this.setState({
              tabIndex: temptabIndex,
              currUrlBase: this.state.tabTitles[temptabIndex],
              info: "",
              currentPage: 1,
            });
          }
        }}
      >
        <TabList>
          <Tab>Cities</Tab>
          <Tab>Companies</Tab>
          <Tab>Jobs</Tab>
        </TabList>
      </Tabs>
    );
  }

  calculatePagination = (instances, info) => {
    var indexOfLastInstance = this.state.numPostPerPage;
    var indexOfFirstInstance = 0;
    var currentInstance = [];

    if (info == "") {
      indexOfLastInstance = this.state.currentPage * this.state.numPostPerPage;
      indexOfFirstInstance = indexOfLastInstance - this.state.numPostPerPage;
      currentInstance = instances.slice(
        indexOfFirstInstance,
        indexOfLastInstance
      );
    }
    return currentInstance;
  };

  render() {
    if (this.state.loading) {
      return <h2 class="text-dark my_text_shadow text-center">Loading ...</h2>;
    }

    return (
      <Container>
        <this.modelsBar />
        <this.renderInstances />
      </Container>
    );
  }
}

export default Search;
