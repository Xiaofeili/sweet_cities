import BubbleChart from "@weknow/react-bubble-chart-d3";
import React, { Component } from "react";
import { Col, Container, Nav, Navbar, Row } from "react-bootstrap";
import axios from "axios";
import { Redirect } from "react-router-dom";
import "../../css/Charts.css";

class MedalsBubble extends Component {
  constructor(props) {
    super(props);
    this.state = {
      olympicsData: [],
    };
  }

  async componentDidMount() {
    axios
      .get("https://api.all-olympian.com/api/olympics")
      .then((response) => {
        this.setState({ olympicsData: response.data });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  createHostedGamesData() {
    var cleanedData = {}; // use object to easily check for hostingCountry membership
    for (var i = 0; i < this.state.olympicsData.length; i++) {
      var game = this.state.olympicsData[i];
      var hostingCountry = game["hosting_country"];
      var season = game["season"];
      if (hostingCountry in cleanedData) {
        cleanedData[hostingCountry]["value"] += 1;
      } else {
        cleanedData[hostingCountry] = {
          label: hostingCountry,
          value: 1,
        };
      }
    }

    // console.log(cleanedData);
    var listData = []; // Recharts input is in list form
    for (var country in cleanedData) {
      listData.push(cleanedData[country]);
    }
    return listData;
  }

  render() {
    return (
      <Container>
        <row>
          <h3 className="text-center">
            Number of Hosted Olympic Games by Country Bubble Chart
          </h3>
        </row>
        <Row>
          <BubbleChart
            graph={{
              zoom: 0.85,
              offsetX: 0.1,
              offsetY: 0,
            }}
            width={1000}
            height={1000}
            padding={0} // optional value, number that set the padding between bubbles
            showLegend={true} // optional value, pass false to disable the legend.
            legendPercentage={20} // number that represent the % of with that legend going to use.
            legendFont={{
              family: "Arial",
              size: 12,
              color: "#000",
              weight: "bold",
            }}
            valueFont={{
              family: "Arial",
              size: 12,
              color: "#fff",
              weight: "bold",
            }}
            labelFont={{
              family: "Arial",
              size: 16,
              color: "#fff",
              weight: "bold",
            }}
            data={this.createHostedGamesData()}
          />
        </Row>
      </Container>
    );
  }
}

export default MedalsBubble;
