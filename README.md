# Sweet Cities 

Group members: <br />

| name           | eid      | gitlab id      |
| -------------- | -------- | -------------- |
| Yuntong Qu     | yq996    | QFREEE         |
| Ben Rehfeld    | bjr2653  | B7nr           |
| Kushal         | ksp887   | kushalsp       |
| Toviyah Liu    | tl24874  | tobytliu       |
| Yichen Zhang   | yz23534  | yichenjake     |


Git SHA: d9cbc5677d8570d1583b4d1ef5b0f92820c56e44

Gitlab pipelines: https://gitlab.com/QFREEE/sweet_cities/pipelines

Website Link: https://thesweetcities.com/

project leader: Yuntong Qu

Phase I <br />

| name           | Estimated completion time      | Actual completion time    |
| -------------- | ------------------------------ | ------------------------- |
| Yuntong Qu     | 10h                            | 12h                       |
| Ben Rehfeld    | 6h                             | 20h                       |
| Kushal         | 8h                             | 11h                       |
| Toviyah Liu    | 8h                             | 12h30min                  |
| Yichen Zhang   | 5h                             | 30h                       |

Phase 2 <br />

| name           | Estimated completion time      | Actual completion time    |
| -------------- | ------------------------------ | ------------------------- |
| Yuntong Qu     | 20h                            | 30h                       |
| Ben Rehfeld    | 15h                            | 20h                       |
| Kushal         | 20h                            | 45h                       |
| Toviyah Liu    | 15h                            | 22h                       |
| Yichen Zhang   | 20h                            | 50h                       |

Phase 3 <br />

| name           | Estimated completion time      | Actual completion time    |
| -------------- | ------------------------------ | ------------------------- |
| Yuntong Qu     | 15h                            | 15h                       |
| Ben Rehfeld    | 15h                            | 20h                       |
| Kushal         | 25h                            | 38h                       |
| Toviyah Liu    | 20h                            | 49h                       |
| Yichen Zhang   | 25h                            | 37h                       |

Phase 4 <br />

| name           | Estimated completion time      | Actual completion time    |
| -------------- | ------------------------------ | ------------------------- |
| Yuntong Qu     | 5h                             | 5h                        |
| Ben Rehfeld    | 5h                             | 5h                        |
| Kushal         | 5h                             | 5h                        |
| Toviyah Liu    | 10h                            | 15h                       |
| Yichen Zhang   | 25h                            | 15h                       |

Comments: