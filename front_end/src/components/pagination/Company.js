import React, { Component } from "react";
import Container from "react-bootstrap/Container";
import Card from "react-bootstrap/Card";
import { CardDeck } from "react-bootstrap";
import { Row, Col } from "react-bootstrap";
import defaultImg from "../../images/default.png";

import Highlight from "react-highlighter";

class Company extends Component {
  constructor(props) {
    super(props);
  }

  fillCard = (instanceObject) => {
    const Tresult = parseInt(instanceObject.instance.company_employeeTotal, 10);
    const Mresult = parseInt(
      instanceObject.instance.company_marketCapitalization,
      10
    );
    const Cresult = parseInt(instanceObject.instance.company_phone, 10);
    const cardDetails = (
      <Card.Text>
        <Highlight search={this.props.search}>State: </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.company_state}
        </Highlight>
        <br />
        <Highlight search={this.props.search}>Total Employees: </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.company_employeeTotal_readable +
            " (" +
            Tresult +
            ")"}
        </Highlight>
        <br />
        <Highlight search={this.props.search}>
          Market Capitalization:{" "}
        </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.company_marketCapitalization_readable +
            " (" +
            Mresult +
            ")"}
        </Highlight>
        <br />
        <Highlight search={this.props.search}>Stock Price: </Highlight>
        <Highlight search={this.props.search}>
          {instanceObject.instance.company_price_readable +
            " (" +
            parseInt(instanceObject.instance.company_price, 10) +
            ")"}
        </Highlight>
        <br />
        <Highlight search={this.props.search}>Contact: </Highlight>
        <Highlight search={this.props.search}>{Cresult}</Highlight>

        <br />
      </Card.Text>
    );

    if (cardDetails == null || cardDetails == "") {
      return <p>card data does not exist</p>;
    }
    return cardDetails;
  };

  tryHandleImage = (instanceObject) => {
    var instanceImgSrc = defaultImg;
    if (
      instanceObject.instance != null &&
      instanceObject.instance.picture_url &&
      instanceObject.instance.picture_url != ""
    ) {
      instanceImgSrc = instanceObject.instance.picture_url;
    }

    return (
      <Card.Img
        width={350}
        height={250}
        src={instanceImgSrc}
        style={{ boxShadow: "1px 1px 15px -5px white" }}
      />
    );
  };

  fillCardTitle = (instanceObject) => {
    const cardTitle = instanceObject.instance.name;

    if (cardTitle == null || cardTitle == "") {
      return <p>Card title does not exist</p>;
    }
    return <Highlight search={this.props.search}>{cardTitle}</Highlight>;
  };

  fillFooter = (instanceObject) => {
    return (
      <div>
        <Row>
          <Col>
            <Card.Link
              href={instanceObject.url + instanceObject.instance.id}
              variant="primary"
              style={{ bottom: 0 }}
            >
              Learn more
            </Card.Link>
          </Col>

          {this.addCompare(instanceObject)}
        </Row>
      </div>
    );
  };

  addCompare = (instanceObject) => {
    if (!this.props.fromSearch) {
      return (
        <Col>
          <Card.Text
            onClick={() => {
              console.log(instanceObject.instance);
              this.props.populateCompareInstances(instanceObject.instance);
            }}
          >
            Compare
          </Card.Text>
        </Col>
      );
    }
    return <></>;
  };

  render() {
    if (this.props.loading) {
      return <h2 class="text-dark my_text_shadow text-center">Loading ...</h2>;
    }

    const url = "/companies/";
    console.log(this.props.Instances);
    return (
      <Container>
        <CardDeck>
          {this.props.Instances.map((instance) => (
            <Col>
              <Card
                key={instance.intance}
                className="bg-dark text-white"
                style={{
                  width: "18rem",
                  height: "32rem",
                  margin: 5,
                  boxShadow: "5px 5px 30px -10px #1743a5",
                }}
              >
                <Card.Body>
                  <this.fillCardTitle instance={instance} />

                  <this.tryHandleImage instance={instance} />

                  <this.fillCard instance={instance} />
                </Card.Body>

                <Card.Footer>
                  <this.fillFooter instance={instance} url={url} />
                </Card.Footer>
              </Card>
            </Col>
          ))}
        </CardDeck>
      </Container>
    );
  }
}
export default Company;
