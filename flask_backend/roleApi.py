import requests
import pprint
from flask import Flask
import json
import sys
from re import search, split
from models import (
    Company,
    City,
    Role,
    app,
    db,
    generate_picture_url,
    generate_video_url,
)

# Key words for software positions
job_keywords = [
    "software",
    "hardware",
    "data",
    "game",
    "database",
    "big data",
    "Web",
    "UI",
    "Information",
]

# Postal to full name
us_state_abbrev = {
    "AL": "Alabama",
    "AK": "Alaska",
    "AZ": "Arizona",
    "AR": "Arkansas",
    "CA": "California",
    "CO": "Colorado",
    "CT": "Connecticut",
    "DE": "Delaware",
    "FL": "Florida",
    "GA": "Georgia",
    "HI": "Hawaii",
    "ID": "Idaho",
    "IL": "Illinois",
    "IN": "Indiana",
    "IA": "Iowa",
    "KS": "Kansas",
    "KY": "Kentucky",
    "LA": "Louisiana",
    "ME": "Maine",
    "MD": "Maryland",
    "MA": "Massachusetts",
    "MI": "Michigan",
    "MN": "Minnesota",
    "MS": "Mississippi",
    "MO": "Missouri",
    "MT": "Montana",
    "NE": "Nebraska",
    "NV": "Nevada",
    "NH": "New Hampshire",
    "NJ": "New Jersey",
    "NM": "New Mexico",
    "NY": "New York",
    "NC": "North Carolina",
    "ND": "North Dakota",
    "OH": "Ohio",
    "OK": "Oklahoma",
    "OR": "Oregon",
    "PA": "Pennsylvania",
    "RI": "Rhode Island",
    "SC": "South Carolina",
    "SD": "South Dakota",
    "TN": "Tennessee",
    "TX": "Texas",
    "UT": "Utah",
    "VT": "Vermont",
    "VA": "Virginia",
    "WA": "Washington",
    "WV": "West Virginia",
    "WI": "Wisconsin",
    "WY": "Wyoming",
}

# Gets response keywords for jobs
def get_kwdata(keyword):
    url = (
        "https://api.careeronestop.org/v1/occupation/Almno7dIEv5hvjW/"
        + keyword
        + "/N/0/10"
    )
    payload = {
        "Authorization": "Bearer fWuowSnK04HdfL4ZFC1u1t8ifS6FgjNHaCVHnXRfU1qSkkfgCeR87JBaIMmOMz2XjmNZ1Y2FS3K6Owr5maYwsw==",
    }

    response = requests.get(url=url, headers=payload)
    if response.status_code == requests.codes.ok:
        return response.json()
    else:
        return response.raise_for_status()


# Retrieves jobs from keyword and occupation codes
def get_kwjobs(keyword_data, occupation_codes):
    keyword_jobs = []
    for occupation_info in keyword_data["OccupationList"]:
        if occupation_info["OnetCode"] not in occupation_codes:
            keyword_jobs.append(occupation_info)
            occupation_codes.append(occupation_info["OnetCode"])
    return keyword_jobs


# Retrieves job codes
def get_occupation_codes():
    occupation_codes = []
    occupation_list = []
    for keyword in job_keywords:
        keyword_data = get_kwdata(keyword)
        keyword_jobs = get_kwjobs(keyword_data, occupation_codes)
        occupation_list += keyword_jobs
    assert len(occupation_codes) == len(occupation_list)
    return occupation_codes


# Gets salary for job
def get_salary(salary_str):
    m = search("(^[0-9]+)(\,([0-9]+)\+?)?", salary_str)
    if "," not in salary_str:
        m = m.group(0)
    else:
        m = m.group(1) + m.group(3)
    return m


# Gets response on jobs from top Cities
def get_top_cities(code):
    url = (
        "https://api.careeronestop.org/v1/jobsearch/Almno7dIEv5hvjW/"
        + code
        + "/US/25/0"
    )
    payload = {
        "Authorization": "Bearer fWuowSnK04HdfL4ZFC1u1t8ifS6FgjNHaCVHnXRfU1qSkkfgCeR87JBaIMmOMz2XjmNZ1Y2FS3K6Owr5maYwsw==",
    }

    response = requests.get(url=url, headers=payload).json()
    locations = response["Locations"]
    num_cities = 5
    counter = 0
    top_cities = ""
    for city in locations:
        city_name = split(",", city["LocationName"]).__getitem__(0)
        if city_name.lower() != "virtual":
            counter += 1
            top_cities += city_name + ". "
        if counter == num_cities:
            break

    state = ""
    for city in locations:
        city_name = split(",", city["LocationName"]).__getitem__(0)
        if city_name.lower() != "virtual":
            state = split(", ", city["LocationName"]).__getitem__(1)
            break

    return top_cities, state


# Gets response on jobs from top Companies
def get_top_companies(code):
    url = (
        "https://api.careeronestop.org/v1/jobsearch/Almno7dIEv5hvjW/"
        + code
        + "/US/25/0"
    )
    payload = {
        "Authorization": "Bearer fWuowSnK04HdfL4ZFC1u1t8ifS6FgjNHaCVHnXRfU1qSkkfgCeR87JBaIMmOMz2XjmNZ1Y2FS3K6Owr5maYwsw==",
    }

    response = requests.get(url=url, headers=payload).json()
    companies = response["Companies"]
    num_companies = 5
    counter = 0
    top_companies_ = ""
    for company in companies:
        company_name = company["CompanyName"]
        if company_name not in top_companies_:
            top_companies_ += company_name + "; "
            counter += 1
        if counter == num_companies:
            break
    return top_companies_


# Populates model with job instances
def get_occupation_info(code):
    # Retrieves response
    url = (
        "https://api.careeronestop.org/v1/occupation/Almno7dIEv5hvjW/"
        + code
        + "/US?training=false&interest=false&videos=false&tasks=false&dwas=true&wages=true&alternateOnetTitles=true&projectedEmployment=false&ooh=false&stateLMILinks=false&relatedOnetTitles=true&skills=true&knowledge=false&ability=false&trainingPrograms=true"
    )
    payload = {
        "Authorization": "Bearer fWuowSnK04HdfL4ZFC1u1t8ifS6FgjNHaCVHnXRfU1qSkkfgCeR87JBaIMmOMz2XjmNZ1Y2FS3K6Owr5maYwsw==",
    }
    response = requests.get(url=url, headers=payload).json()
    try:
        # Pulls Attributes from response
        title = response["OccupationDetail"][0]["OnetTitle"]
        description = response["OccupationDetail"][0]["OnetDescription"]
        wages = response["OccupationDetail"][0]["Wages"]["NationalWagesList"]
        if wages[0]["RateType"] == "Hourly":
            wages = wages[1]
        else:
            wages = wages[0]

        salary = get_salary(wages["Pct10"])
        low_salary = int(salary)

        salary = get_salary(wages["Median"])
        median_salary = int(salary)

        salary = get_salary(wages["Pct90"])
        high_salary = int(salary)

        daily_responsibility = None
        dwas = response["OccupationDetail"][0]["Dwas"]
        daily_responsibility = ""
        for dwa in dwas:
            if float(dwa["DataValue"]) > 3.5:
                daily_responsibility += dwa["DwaTitle"] + " "

        alt_titles = None
        alternative_titles = response["OccupationDetail"][0]["AlternateTitles"]
        alt_titles = ""
        for alt_title in alternative_titles:
            alt_titles += alt_title + ". "

        top_cities, role_state = get_top_cities(code)
        role_state = us_state_abbrev[role_state]
        top_companies = get_top_companies(code)

        skills = None
        skills_list = response["OccupationDetail"][0]["SkillsDataList"]
        skills = ""
        for skill in skills_list:
            if float(skill["DataValue"]) > 3.5:
                skills += skill["ElementName"] + ". "

        trainings = None
        training_programs = response["OccupationDetail"][0]["TrainingPrograms"]
        trainings = ""
        if training_programs:
            for prog in training_programs:
                trainings += prog + ". "
        # Populates model instance with attributes
        db.session.add(
            Role(
                name=title,
                role_description=description,
                low_salary=low_salary,
                median_salary=median_salary,
                high_salary=high_salary,
                daily_responsibility=daily_responsibility,
                alternative_titles=alt_titles,
                top_cities=top_cities,
                top_companies=top_companies,
                skills=skills,
                trainings=trainings,
                role_state=role_state,
                picture_url="",
                video_url="",
            )
        )
        db.session.commit()

    except KeyError:
        pass


# Populates Job model with job instances
def job_api():
    db.create_all()
    occupation_codes = get_occupation_codes()
    for code in occupation_codes:
        get_occupation_info(code)


# Executes Script
if __name__ == "__main__":
    job_api()
