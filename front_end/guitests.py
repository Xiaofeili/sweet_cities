import os
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

"""
options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/chromium"
driver = webdriver.Chrome(chrome_options=options)
driver.get('https://python.org')
"""

"""
chrome_options = Options()
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.binary_location = "./chromedriver"
exec_path = os.path.join(os.getcwd(), "chromedriver")
driver = webdriver.Chrome(options=chrome_options, executable_path=exec_path)
driver.get('https://python.org')
"""

# test gui with Selenium
class Test_GUI(unittest.TestCase):
    def setUp(self):
        exec_path = os.path.join(os.getcwd(), "geckodriver")
        self.driver = webdriver.Firefox(executable_path=exec_path)
        self.driver.get('https://thesweetcities.com/')

    def tearDown(self):
        self.driver.quit()

    def test_splash_cities_btn(self):
        cities_btn = self.driver.find_element_by_link_text('Explore our cities!')
        cities_btn.click()
        page_header = self.driver.find_element_by_css_selector('div.bg h1')
        self.assertEqual(page_header.text, 'Cities')

    def test_splash_companies_btn(self):
        companies_btn = self.driver.find_element_by_link_text('Explore our companies!')
        companies_btn.click()
        page_header = self.driver.find_element_by_css_selector('div.bg h1')
        self.assertEqual(page_header.text, 'Companies')

    def test_jobs_companies_btn(self):
        jobs_btn = self.driver.find_element_by_link_text('Explore our jobs!')
        jobs_btn.click()
        page_header = self.driver.find_element_by_css_selector('div.bg h1')
        self.assertEqual(page_header.text, 'Jobs')

    def test_all_splash_links(self):
        links = self.driver.find_elements_by_tag_name('a')
        for link in links:
            url = link.get_attribute('href')
            self.assertIsNotNone(url)
            self.assertNotEqual(url, '')

    # tests if profile images in about page work by looking for a 404 error (i.e. the h1)
    def test_about_images(self):
        about_link = self.driver.find_element_by_link_text('About')
        about_link.click()
        profile_images = self.driver.find_elements_by_css_selector('div.avatar-wrapper img')
        image_urls = [image.get_attribute('src') for image in profile_images]
        for image_url in image_urls:
            self.driver.get(image_url)
            error_code = self.driver.find_elements_by_css_selector('div.bg > h1')
            self.assertEqual(len(error_code), 0)

    def test_nav_cities(self):
        companies_btn = self.driver.find_element_by_link_text('Cities')
        companies_btn.click()
        page_header = self.driver.find_element_by_css_selector('h1.text-dark.my_text_shadow.text-center')
        self.assertEqual(page_header.text, 'Cities')

    def test_nav_companies(self):
        companies_btn = self.driver.find_element_by_link_text('Companies')
        companies_btn.click()
        page_header = self.driver.find_element_by_css_selector('h1.text-dark.my_text_shadow.text-center')
        self.assertEqual(page_header.text, 'Companies')

    def test_city_search(self):
        companies_btn = self.driver.find_element_by_link_text('Cities')
        companies_btn.click()
        search_input = self.driver.find_element_by_xpath("//*[@placeholder='Search']")
        search_input.send_keys("Houston")
        time.sleep(1)
        search_btn = self.driver.find_element_by_css_selector('form button')
        search_btn.click()
        # Selenium is too fast, tries to find element before its loaded
        time.sleep(3)
        card_name = self.driver.find_element_by_css_selector('div.card-body mark.highlight')
        self.assertEqual(card_name.get_attribute('innerText'), 'Houston')

    def test_companies_search(self):
        companies_btn = self.driver.find_element_by_link_text('Companies')
        companies_btn.click()
        search_input = self.driver.find_element_by_xpath("//*[@placeholder='Search']")
        search_input.send_keys("Oragenics Inc")
        time.sleep(1)
        search_btn = self.driver.find_element_by_css_selector('form button')
        search_btn.click()
        # Selenium is too fast, tries to find element before its loaded
        time.sleep(3)
        card_name = self.driver.find_element_by_css_selector('div.card-body mark.highlight')
        self.assertEqual(card_name.get_attribute('innerText'), 'Oragenics Inc')

    def test_jobs_search(self):
        companies_btn = self.driver.find_element_by_link_text('Job Roles')
        companies_btn.click()
        search_input = self.driver.find_element_by_xpath("//*[@placeholder='Search']")
        search_input.send_keys("Database Architects")
        time.sleep(1)
        search_btn = self.driver.find_element_by_css_selector('form button')
        search_btn.click()
        # Selenium is too fast, tries to find element before its loaded
        time.sleep(3)
        card_name = self.driver.find_element_by_css_selector('div.card-body mark.highlight')
        self.assertEqual(card_name.get_attribute('innerText'), 'Database Architects')

    def test_nav_jobs(self):
        companies_btn = self.driver.find_element_by_link_text('Job Roles')
        companies_btn.click()
        page_header = self.driver.find_element_by_css_selector('h1.text-dark.my_text_shadow.text-center')
        self.assertEqual(page_header.text, 'Jobs')

    def test_jobs_instance(self):
        companies_btn = self.driver.find_element_by_link_text('Job Roles')
        companies_btn.click()
        search_input = self.driver.find_element_by_xpath("//*[@placeholder='Search']")
        search_input.send_keys("Web Developers")
        time.sleep(1)
        search_btn = self.driver.find_element_by_css_selector('form button')
        search_btn.click()
        # Selenium is too fast, tries to find element before its loaded
        time.sleep(3)
        instance_link = self.driver.find_element_by_link_text('Learn more')
        instance_link.click()
        instance_header = self.driver.find_element_by_css_selector('h1.text-dark.my_text_shadow')
        self.assertEqual(instance_header.text, 'Web Developers')

    def test_companies_instance(self):
        companies_btn = self.driver.find_element_by_link_text('Companies')
        companies_btn.click()
        search_input = self.driver.find_element_by_xpath("//*[@placeholder='Search']")
        search_input.send_keys("New Mountain Finance Corp")
        time.sleep(1)
        search_btn = self.driver.find_element_by_css_selector('form button')
        search_btn.click()
        time.sleep(3)
        instance_link = self.driver.find_element_by_link_text('Learn more')
        instance_link.click()
        instance_header = self.driver.find_element_by_css_selector('h1.text-dark.my_text_shadow')
        self.assertEqual(instance_header.text, 'New Mountain Finance Corp')

    def test_cities_instance(self):
        companies_btn = self.driver.find_element_by_link_text('Cities')
        companies_btn.click()
        search_input = self.driver.find_element_by_xpath("//*[@placeholder='Search']")
        search_input.send_keys("Detroit")
        time.sleep(1)
        search_btn = self.driver.find_element_by_css_selector('form button')
        search_btn.click()
        time.sleep(3)
        card_name = self.driver.find_element_by_css_selector('div.card-body mark.highlight')
        self.assertEqual(card_name.text, 'Detroit')

    def test_cities_filter(self):
        model_btn = self.driver.find_element_by_link_text('Cities')
        model_btn.click()
        time.sleep(2)

        filter_btn = self.driver.find_element_by_id('filter-attribute')
        filter_btn.click()
        time.sleep(2)

        index_btn = self.driver.find_element_by_link_text('Climate')
        index_btn.click()

        filter_value_btn = self.driver.find_element_by_id('filter-value')
        filter_value_btn.click()
        index_value_btn = self.driver.find_element_by_link_text('MEDIUM')
        index_value_btn.click()

        time.sleep(2)

        card_index = self.driver.find_element_by_css_selector('div.card-body p.card-text span:nth-child(5)')
        self.assertEqual(card_index.text, 'MEDIUM (50)')

if __name__ == "__main__": # pragma: no cover
    unittest.main()
