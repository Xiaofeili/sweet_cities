import sys
import os

from flask import Flask, request, Response, url_for, abort
from sqlalchemy import create_engine, or_, func, desc,union_all,distinct
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import sessionmaker
from markupsafe import escape
from functools import reduce
import json
import pprint
import requests

from flask_cors import CORS
from models import (
    engine,
    City,
    Role,
    Company,
    db,
    application
)

application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
CORS(application)
DBSession = sessionmaker(bind=engine)
db.metadata.bind = engine
global session
session = DBSession()


@application.route("/", methods=["GET"])
def hello():
    return "api endpoint for thesweetcities.com"

#include role, company
@application.route("/cities", methods=["GET"])
def getAllCities():

    return instancesHandle(City, request)

# include github_id 
@application.route("/companies", methods=["GET"])
def getAllCompanies():

    return instancesHandle(Company,request)

# include city, company 
@application.route("/roles", methods=["GET"])
def getAllRoles():
  
    return instancesHandle(Role,request)
    
@application.route("/cities/<int:id>", methods=["GET"])
def getOneCity(id):
    try:
        city_instance = session.query(City).filter_by(id=id).first()
        city_dict = instance2dict(city_instance)
        # companies
        companies_ids = (
                session.query(Company)
                    .filter(Company.company_state == city_instance.city_state)
                    .limit(3)
                    .all()
            )
        city_dict.update({"companies_id":getUniqueReturn(companies_ids)})

        # roles
        roles_ids = (
                session.query(Role)
                    .filter(Role.role_state == city_instance.city_state)
                    .limit(3)
                    .all()
            )
        city_dict.update({"roles_id":getUniqueReturn(roles_ids)})

        # similar roles
        similar_roles_ids = (
            session.query(Role)
                .filter(Role.role_state == city_instance.city_state)
                .all()
        )
        similar_dict = {"roles_id": getUniqueReturn(similar_roles_ids)}

        # similar companies
        similar_companies_ids = (
            session.query(Company)
                .filter(Company.company_state == city_instance.city_state)
                .all()
        )
        similar_dict.update({"companies_id": getUniqueReturn(similar_companies_ids)})
        city_dict.update({"similar": similar_dict})

        return response(city_dict)
    except:
        session.rollback()
        return response(None)


@application.route("/companies/<int:id>", methods=["GET"])
def getOneCompany(id):
    id = int(id)
  
    company_instance = session.query(Company).filter_by(id=id).first()
    company_dict = instance2dict(company_instance)

    # city_id 
    cities_id = (
            session.query(City)
                .filter(City.city_state == company_instance.company_state)
                .limit(3)
                .all()
        )
    company_dict.update({"cities_id":getUniqueReturn(cities_id)})

    # roles
    roles_ids = (
            session.query(Role)
                .filter(Role.role_state == company_instance.company_state)
                .limit(3)
                .all()
        )
    company_dict.update({"roles_id":getUniqueReturn(roles_ids)})

    # similar cities
    similar_cities_id = (
        session.query(City)
            .filter(City.city_state == company_instance.company_state)
            .all()
    )
    similar_dict = {"cities_id": getUniqueReturn(similar_cities_id)}

    # similar roles
    similar_roles_ids = (
        session.query(Role)
            .filter(Role.role_state == company_instance.company_state)
            .all()
    )
    similar_dict.update({"roles_id": getUniqueReturn(similar_roles_ids)})
    company_dict.update({"similar": similar_dict})

    return response(company_dict)

@application.route("/roles/<int:id>", methods=["GET"])
def getOneRole(id):
    role_instance = session.query(Role).filter_by(id=id).first()
    role_dict = instance2dict(role_instance)

    # companies_id
    companies_ids = (
        session.query(Company)
            .filter(Company.company_state == role_instance.role_state)
            .limit(3)
            .all()
    )
    role_dict.update({"companies_id":getUniqueReturn(companies_ids)})

    # cites_id
    cities_id = (
            session.query(City)
                .filter(City.city_state == role_instance.role_state)
                .limit(3)
                .all()
    )
    role_dict.update({"cities_id":getUniqueReturn(cities_id)})

    # similar cities ids
    similar_cities_id = (
        session.query(City)
            .filter(City.city_state == role_instance.role_state)
            .all()
    )
    similar_dict = {"cities_id": getUniqueReturn(similar_cities_id)}

    # similar company ids
    similar_companies_ids = (
        session.query(Company)
            .filter(Company.company_state == role_instance.role_state)
            .all()
    )
    similar_dict.update({"companies_id": getUniqueReturn(similar_companies_ids)})
    role_dict.update({"similar": similar_dict})

    return response(role_dict)


"""
Utility method for application.py
"""

def instancesHandle(instance_model, request):
    '''
    support sorted_by, order_by, {{column}}
    '''

    all_instances = instance_model.query

    if request.args:
        # have incoming query params
        args = request.args # dict of args 
        sorted_by = request.args.get('sorted_by',None)
        order_by = request.args.get('order_by','asc')
        searched_by = request.args.get('searched_by',None)
    
        if searched_by:
            all_instances = searchModle(instance_model, searched_by)

        for k,v in args.items():
            if k != 'sorted_by' and k != 'order_by' and k!='searched_by':
                # handle {{column}}
                all_instances = all_instances.filter(getattr(instance_model, k)==v)
        

        if sorted_by:
            # handle sorted and order 
            col = getattr(instance_model, sorted_by)
            if order_by == 'asc':
                all_instances = all_instances.order_by(col.asc()).all()
            else:
                all_instances = all_instances.order_by(col.desc()).all()
    
    all_instances_list = list(map(instance2dict,all_instances))
    return response(all_instances_list)
    

def searchModle ( model, search_string): 

    query = session.query(model).all()
    found_query = []
    for each_row in query:
        if search_string.lower() in str(each_row).lower():
            found_query.append(each_row.id)
    
    return session.query(model).filter(model.id.in_(found_query))
    
def instance2dict(instance):
    instance_dict = instance.__dict__
    instance_dict.pop("_sa_instance_state", None)
    for k,v in instance_dict.items():
        if is_number(v):
            instance_dict[k] = str("{0:.2f}".format(float(v)))
    return instance_dict

def getUniqueReturn(instances):
    res = []
    try:
        for instance in instances:
            res.append(instance.id)
    except TypeError:
        pass
    return res


def is_number(s):
    try:
        float(s)
        return True
    except (ValueError, TypeError):
        pass
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    return False

def response(obj):
    response = Response(json.dumps(obj), status="200")
    response.status_code = 200
    response.headers.add("content-type", "application/json")

    return response


if __name__ == "__main__":
    application.run(debug=True)
        