import React from "react";
import { Container, Row, Col } from "react-bootstrap";

import Figure from "react-bootstrap/Figure";
import { Button, ButtonToolbar } from "react-bootstrap";

import "../../css/About.css";
import Toby from "../../images/toby.JPG";
import Kushal from "../../images/kushal.jpg";
import Ben from "../../images/ben.jpg";
import Yuntong from "../../images/yuntong.jpg";
import Yichen from "../../images/yichen.jpg";
import COMMIT_SVG from "../../images/commit.svg";
import ISSUE_SVG from "../../images/issue.svg";
import UNITTEST_SVG from "../../images/unittest.svg";
import AWS from "../../images/aws.png";
import POSTMAN from "../../images/postman.jpg";
import GITLAB from "../../images/gitlab.png";
import REACT from "../../images/react.svg";
import SLACK from "../../images/slack.png";
import BOOTSTRAP from "../../images/bootstrap.png";
import FLASK from "../../images/flask.png";
import POSTGRESQL from "../../images/postgresql.png";
import MOCHA from "../../images/mocha.png";
import SELENIUM from "../../images/selenium.png";

import Card from "react-bootstrap/Card";
import ReactPlayer from "react-player";

const GOOGLE_MAP_API =
  "https://developers.google.com/maps/documentation/maps-static/intro";
const METAWEATHER_API = "https://www.metaweather.com/api/";
const GITHUB_JOBS_API = "https://jobs.github.com/api";
const UNSPLASH_IMAGE_API = "https://unsplash.com/developers";
const NUMBEO_API = "https://www.numbeo.com/";
const FINNHUB_API = "https://finnhub.io/";

const GITLAB_REPO_URL = "https://gitlab.com/QFREEE/sweet_cities.git";
const GITLAB_ISSUE_URL = "https://gitlab.com/QFREEE/sweet_cities/issues";
const GITLAB_ISSUES_API =
  "https://gitlab.com/api/v4/projects/17040102/issues?per_page=100&page=";
const GITLAB_COMMITS_API =
  "https://gitlab.com/api/v4/projects/17040102/repository/commits?per_page=100&page=";

const SWEET_CITIES_API =
  "https://documenter.getpostman.com/view/8809906/SzKWuxeh?version=latest";

class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      author_names: [
        "Toviyah Liu",
        "Kushalsp",
        "Ben Rehfeld",
        "Yuntong Qu",
        "Yichen Zhang",
      ],
      user_names: ["tobytliu", "Pachchigar", "B7nr", "QFREEE", "yichenjake"],
      commits: [0, 0, 0, 0, 0],
      issues: [0, 0, 0, 0, 0],
      unittests: [20, 0, 0, 7, 5],

      totalCommits: 0,
      totalIssues: 0,
      totalUnitTests: 0,

      otherIssues: 0,
    };
  }

  getCommits = (response) => {
    let totalCommits = this.state.totalCommits;
    let commits = this.state.commits;
    response.forEach((commit) => {
      totalCommits++;
      switch (commit.author_name) {
        case "Toby":
        case this.state.author_names[0]:
          commits[0]++;
          break;
        case this.state.author_names[1]:
          commits[1]++;
          break;
        case this.state.author_names[2]:
          commits[2]++;
          break;
        case this.state.author_names[3]:
        case "Yuntong X5v6":
          commits[3]++;
          break;
        case this.state.author_names[4]:
          commits[4]++;
          break;
        default:
          break;
      }
    });
    this.setState({ totalCommits: totalCommits });
    this.setState({ commits: commits });
    
  };

  getIssues = (response) => {
    let totalIssues = this.state.totalIssues;
    let issues = this.state.issues;
    let otherIssues = this.state.otherIssues;
    response.forEach((issue) => {
      totalIssues++;
      if (issue.closed_by != null) {
        switch (issue.closed_by.username) {
          case this.state.user_names[0]:
            issues[0]++;
            break;
          case this.state.user_names[1]:
            issues[1]++;
            break;
          case this.state.user_names[2]:
            issues[2]++;
            break;
          case this.state.user_names[3]:
            issues[3]++;
            break;
          case this.state.user_names[4]:
            issues[4]++;
            break;
          default:
            break;
        }
      }
    });

    this.setState({ totalIssues: totalIssues });
    this.setState({ issues: issues });
    this.setState({ otherIssues: otherIssues });
  };

  async componentDidMount() {
    let commitsPage = 1;
    let commitsResponse = await (
      await fetch(GITLAB_COMMITS_API + commitsPage)
    ).json();
    while (commitsResponse != 0) {
      this.getCommits(commitsResponse);
      commitsPage += 1;
      commitsResponse = await (
        await fetch(GITLAB_COMMITS_API + commitsPage)
      ).json();
    }

    let issuesPage = 1;
    let issuesResponse = await (
      await fetch(GITLAB_ISSUES_API + issuesPage)
    ).json();
    while (issuesResponse != 0) {
      this.getIssues(issuesResponse);
      issuesPage += 1;
      issuesResponse = await (
        await fetch(GITLAB_ISSUES_API + issuesPage)
      ).json();
    }
  }

  render() {
    return (
      <Container>
        <br />
        <h1 className="h1-responsive text-center text-dark">Our Team</h1>
        <br />
        <p className="grey-text w-responsive mx-auto mb-5 text-center">
          Provide models for graduating or presently working software engineers
          to learn about the best places to live and work. Engineers will be
          able to consult our website to learn about competitive wages, cities
          with the highest quality of living, companies with the best perks, and
          industry growth.
        </p>

        <div className="row">
          <div className="col-lg-6 col-md-6 mb-lg-0 mb-5">
            <div className="avatar-wrapper mx-auto">
              <Figure.Image
                className="logo-hover"
                src={Toby}
                alt="Toby Liu"
                width={275}
              />
            </div>
            <div className="description">
              <h5 className="text-dark mt-4 mb-3">Toby Liu</h5>
              <p className="text-uppercase text-primary font-weight-bold">
                Frontend
              </p>
              <p className="text-primary font-weight-bold">
                I am a front-end developer. I created a new splash page city
                vector art, restyled components, and wrote Selenium GUI/mocha
                component tests.
              </p>
              <p className="grey-text">
                I'm a 3rd year interested in tinkering with full stack and
                useless front-end trinkets. Groovy things I occupy my time with
                inculde: bouldering, cooking, and reading. Programming is also
                sometimes fun.
              </p>
            </div>
            <div className="row">
              <div className="col"></div>
              <div className="col-6">
                <a className="logo-link" href={GITLAB_REPO_URL}>
                  &emsp; &emsp;
                  <img
                    className="logo-hover"
                    src={COMMIT_SVG}
                    alt="Commit Logo"
                  />
                  &emsp;
                  {this.state.commits[0]}
                  &emsp;
                </a>

                <a className="logo-link" href={GITLAB_ISSUE_URL}>
                  <img
                    className="logo-hover"
                    src={ISSUE_SVG}
                    alt="Issue Logo"
                  />
                  &emsp;
                  {this.state.issues[0]}
                  &emsp;
                </a>

                <a className="logo-link">
                  <img
                    className="logo-hover"
                    src={UNITTEST_SVG}
                    alt="Unittest Logo"
                  />
                  &emsp;
                  {this.state.unittests[0]}
                  &emsp;
                </a>
              </div>

              <div className="col"></div>
            </div>
          </div>

          <div
            className="col-lg-6 col-md-6 mb-lg-0 mb-5"
          >
            <div className="avatar-wrapper mx-auto">
              <Figure.Image
                className="logo-hover"
                src={Kushal}
                alt="Kushal Pachchigar"
                width={275}
              />
            </div>
            <div className="description">
              <h5 className="text-dark mt-4 mb-3">Kushal Pachchigar</h5>
              <p className="text-uppercase text-primary font-weight-bold">
                Frontend &amp; API
              </p>
              <p className="text-primary font-weight-bold">
                Frontend -- designed and implemented models and instance pages. Implemented multimedia on the instance
                pages. Implemented the frontend handling of features including searching, sorting, ordering, filtering, and comparing. I usually make big change commits.
              </p>
              <p className="grey-text">
                I'm an international student from India, joined UT in spring
                2017 as undeclared major, later transferred to Computer Science
                in Fall 2018.
              </p>
            </div>
            <div className="row">
              <div className="col"></div>

              <div className="col-6">
                <a className="logo-link" href={GITLAB_REPO_URL}>
                  &emsp; &emsp;
                  <img
                    className="logo-hover"
                    src={COMMIT_SVG}
                    alt="Commit Logo"
                  />
                  &emsp;
                  {this.state.commits[1]}
                  &emsp;
                </a>

                <a className="logo-link" href={GITLAB_ISSUE_URL}>
                  <img
                    className="logo-hover"
                    src={ISSUE_SVG}
                    alt="Issue Logo"
                  />
                  &emsp;
                  {this.state.issues[1]}
                  &emsp;
                </a>

                <a className="logo-link">
                  <img
                    className="logo-hover"
                    src={UNITTEST_SVG}
                    alt="Unittest Logo"
                  />
                  &emsp;
                  {this.state.unittests[1]}
                  &emsp;
                </a>
              </div>
              <div className="col"></div>
            </div>
          </div>
          <footer>
            <br />
            <br />
            <br />
            <br />
          </footer>
        </div>

        <div className="row">
          <div
            className="col-lg-4 col-md-6 mb-lg-0 mb-5"
          >
            <div className="avatar-wrapper mx-auto">
              <Figure.Image
                className="logo-hover"
                src={Ben}
                alt="Ben Rehfeld"
                width={275}
              />
            </div>
            <div className="description">
              <h5 className="text-dark mt-4 mb-3">Ben Rehfeld</h5>
              <p className="text-uppercase text-primary font-weight-bold">
                Backend &amp; Dev Ops
              </p>
              <p className="text-primary font-weight-bold">
                I maintain the AWS hostings, and mostly work on the back end and
                the database. I also have touched the API and Front end a
                little. I commit only when I know I have the issue completely
                working.
              </p>
              <p className="grey-text">
                Hi, I am a third year student pursuing a Bachelor of Science in
                Computer Science. I am from Houston, TX and I enjoy climbing.
              </p>
            </div>
            <div className="row">
              <div className="col"></div>
              <div className="col-8">
                <a className="logo-link" href={GITLAB_REPO_URL}>
                  &emsp;
                  <img
                    className="logo-hover"
                    src={COMMIT_SVG}
                    alt="Commit Logo"
                  />
                  &emsp;
                  {this.state.commits[2]}
                  &emsp;
                </a>

                <a className="logo-link" href={GITLAB_ISSUE_URL}>
                  <img
                    className="logo-hover"
                    src={ISSUE_SVG}
                    alt="Issue Logo"
                  />
                  &emsp;
                  {this.state.issues[2]}
                  &emsp;
                </a>

                <a className="logo-link">
                  <img
                    className="logo-hover"
                    src={UNITTEST_SVG}
                    alt="Unittest Logo"
                  />
                  &emsp;
                  {this.state.unittests[2]}
                  &emsp;
                </a>
              </div>
              <div className="col"></div>
            </div>
          </div>

          <div
            className="col-lg-4 col-md-6 mb-lg-0 mb-5"
          >
            <div className="avatar-wrapper mx-auto">
              <Figure.Image
                className="logo-hover"
                src={Yuntong}
                alt="Yuntong Qu"
                width={275}
              />
            </div>
            <div className="description">
              <h5 className="text-dark mt-4 mb-3">Yuntong Qu</h5>
              <p className="text-uppercase text-primary font-weight-bold">
                Backend
              </p>

              <p className=" text-primary font-weight-bold">
                I mainly worked backend routing and database. I commit
                frequently.
              </p>
              <p className="grey-text">
                I am a third year CS student. I enjoy poker, Dota2 and swimming.
              </p>
            </div>
            <div className="row">
              <div className="col"></div>
              <div className="col-8">
                <a className="logo-link" href={GITLAB_REPO_URL}>
                  &emsp;
                  <img
                    className="logo-hover"
                    src={COMMIT_SVG}
                    alt="Commit Logo"
                  />
                  &emsp;
                  {this.state.commits[3]}
                  &emsp;
                </a>

                <a className="logo-link" href={GITLAB_ISSUE_URL}>
                  <img
                    className="logo-hover"
                    src={ISSUE_SVG}
                    alt="Issue Logo"
                  />
                  &emsp;
                  {this.state.issues[3]}
                  &emsp;
                </a>

                <a className="logo-link">
                  <img
                    className="logo-hover"
                    src={UNITTEST_SVG}
                    alt="Unittest Logo"
                  />
                  &emsp;
                  {this.state.unittests[3]}
                  &emsp;
                </a>
              </div>
              <div className="col"></div>
            </div>
          </div>

          <div
            className="col-lg-4 col-md-6 mb-lg-0 mb-5"
          >
            <div className="avatar-wrapper mx-auto">
              <Figure.Image
                className="logo-hover"
                src={Yichen}
                alt="Yichen Zhang"
                width={275}
              />
            </div>
            <div className="description">
              <h5 className="text-dark mt-4 mb-3">Yichen Zhang</h5>
              <p className="text-uppercase text-primary font-weight-bold">
                Full Stack
              </p>
              <p className="text-primary font-weight-bold">
                I work mostly on the front end and database. Depending on things
                I work on, I make big and small commits.
              </p>
              <p className="grey-text">
                I'm a third year Computer Science major at UT Austin.
              </p>
            </div>
            <div className="row">
              <div className="col"></div>
              <div className="col-8">
                <a className="logo-link" href={GITLAB_REPO_URL}>
                  &emsp;
                  <img
                    className="logo-hover"
                    src={COMMIT_SVG}
                    alt="Commit Logo"
                  />
                  &emsp;
                  {this.state.commits[4]}
                  &emsp;
                </a>

                <a className="logo-link" href={GITLAB_ISSUE_URL}>
                  <img
                    className="logo-hover"
                    src={ISSUE_SVG}
                    alt="Issue Logo"
                  />
                  &emsp;
                  {this.state.issues[4]}
                  &emsp;
                </a>

                <a className="logo-link">
                  <img
                    className="logo-hover"
                    src={UNITTEST_SVG}
                    alt="Unittest Logo"
                  />
                  &emsp;
                  {this.state.unittests[4]}
                  &emsp;
                </a>
              </div>
              <div className="col"></div>
            </div>
          </div>
        </div>

        <ButtonToolbar className="mx-4">
          <Button
            className="topbtn mr-5"
            href={GITLAB_REPO_URL}
            variant="outline-secondary"
          >
            GitLab
          </Button>
          <Button
            className="topbtn ml-4"
            href={SWEET_CITIES_API}
            variant="outline-secondary"
          >
            API
          </Button>
        </ButtonToolbar>

        <footer>
          <br />
          <br />
          <hr></hr>
          <ReactPlayer
            style={{
              marginLeft: "20%",
              marginTop: "2.5%",
              boxShadow: "5px 5px 30px -10px blue",
            }}
            url="https://www.youtube.com/embed/_pLpZvoyAKw"
          />
          <Card.Text
            style={{ textAlign: "center", marginTop: "1%", marginBottom: "1%" }}
          >
            How To Use our Website!
          </Card.Text>
          <hr></hr>
          <br />
          <br />
        </footer>

        <div>
          <h2 className="description">Data Source</h2>
          <p className="description">
            Our team made use of <a href={GOOGLE_MAP_API}>Google Map</a>,
            &thinsp;
            <a href={METAWEATHER_API}>metaweather</a>, &thinsp;
            <a href={GITHUB_JOBS_API}>Github Jobs</a>, &thinsp;
            <a href={UNSPLASH_IMAGE_API}>Unsplash</a>
          </p>
          <p className="description">
            <a href={NUMBEO_API}>Numbeo</a>, &thinsp;
            <a href={FINNHUB_API}>Finnhub</a>
          </p>
          <h2 className="description">Tools</h2>
        </div>

        <Container>
          <Row>
            <Col className="tool-container" xs={6} md={4}>
              <a href="https://aws.amazon.com/">
                <img
                  className="tool-logo"
                  src={AWS}
                  alt="Amazon Web Services"
                ></img>
              </a>
            </Col>

            <Col className="tool-container" xs={6} md={4}>
              <a href="https://www.postman.com/">
                <img className="tool-logo" src={POSTMAN} alt="Postman"></img>
              </a>
            </Col>

            <Col className="tool-container" xs={6} md={4}>
              <a href="https://gitlab.com/">
                <img className="tool-logo" src={GITLAB} alt="GitLab"></img>
              </a>
            </Col>
          </Row>
          <br />

          <Row>
            <Col className="tool-container" xs={6} md={4}>
              <a href="https://reactjs.org/">
                <img className="tool-logo" src={REACT} alt="React"></img>
              </a>
            </Col>

            <Col className="tool-container" xs={6} md={4}>
              <a href="https://slack.com/">
                <img className="tool-logo" src={SLACK} alt="Slack"></img>
              </a>
            </Col>

            <Col className="tool-container" xs={6} md={4}>
              <a href="https://getbootstrap.com/">
                <img
                  className="tool-logo"
                  src={BOOTSTRAP}
                  alt="Bootstrap"
                ></img>
              </a>
            </Col>
          </Row>
          <br />

          <Row>
            <Col className="tool-container" xs={6} md={4}>
              <a href="https://flask.palletsprojects.com/en/1.1.x/">
                <img className="tool-logo" src={FLASK} alt="Flask"></img>
              </a>
            </Col>

            <Col className="tool-container" xs={6} md={4}>
              <a href="https://www.postgresql.org/">
                <img
                  className="tool-logo"
                  src={POSTGRESQL}
                  alt="PostgreSQL"
                ></img>
              </a>
            </Col>

            <Col className="tool-container" xs={6} md={4}>
              <a href="https://mochajs.org/">
                <img className="tool-logo" src={MOCHA} alt="Mocha"></img>
              </a>
            </Col>
          </Row>
          <br />
          <Row>
            <Col className="tool-container" xs={6} md={4}>
              <a href="https://www.selenium.dev/">
                <img className="tool-logo" src={SELENIUM} alt="Selenium"></img>
              </a>
            </Col>
          </Row>
          <br />
          <br />
        </Container>
      </Container>
    );
  }
}

export default About;
