import React from "react";
import * as d3 from "d3";
import axios from "axios";
import { Redirect } from "react-router-dom";

import "../../css/JobsBarChart.css";

class JobsBubbleChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jobsInfo: null,
    };
  }

  async componentDidMount() {
    const url = "https://api.thesweetcities.com/roles";
    axios
      .get(url)
      .then((response) => {
        this.setState({ jobsInfo: response.data });
      })
      .catch(() => {
        Redirect("/404");
      });
  }

  componentDidUpdate() {
    var svg = d3.select("svg"),
      width = +svg.attr("width"),
      height = +svg.attr("height");

    // Define the div for the tooltip
    var div = d3
      .select("body")
      .append("div")
      .attr("class", "tooltip")
      .text("")
      .style("fill", "#69b3a2");

    svg
      .append("text")
      .attr("x", 100)
      .attr("y", 0)
      .attr("dy", "3.5em")
      .attr("text-anchor", "start")
      .style("font-size", "28px")
      .style("font-weight", "bold")
      .text("Displays the median salary of each job");

    var pack = d3
      .pack()
      .size([width - 500, height])
      .padding(1.5);

    var data = [];
    for (let key in this.state.jobsInfo) {
      var job = this.state.jobsInfo[key];
      data.push({
        name: job["name"],
        median_salary: Number(job["median_salary"]),
      });
    }

    var color = d3
      .scaleOrdinal()
      .domain(
        data.map((job) => {
          return job.name;
        })
      )
      .range([
        "#fbb4ae",
        "#b3cde3",
        "#ccebc5",
        "#decbe4",
        "#fed9a6",
        "#ffe9a8",
        "#b9bfe3",
        "#fddaec",
        "#cccccc",
      ]);

    var root = d3.hierarchy({ children: data }).sum(function (d) {
      return d.median_salary;
    });

    var node = svg
      .selectAll(".node")
      .data(pack(root).leaves())
      .enter()
      .append("g")
      .attr("class", "node")
      .attr("transform", function (d) {
        return "translate(" + d.x + "," + d.y + ")";
      });

    node
      .append("circle")
      .attr("id", function (d) {
        return d.name;
      })
      .attr("r", function (d) {
        return d.r;
      })
      .style("fill", function (d) {
        return color(d.data.name);
      })
      .on("mouseover", function (d) {
        div.transition().duration(200).style("opacity", 0.9);
        d3.select(this).transition().duration("50").attr("opacity", ".85");
        var duration = 300;
        data.forEach(function (d, i) {
          console.log(d.median_salary);
          node
            .transition()
            .duration(duration)
            .delay(i * duration)
            .attr("r", d.median_salary);
        });
        div
          .html(d.data.name + ": <br>" + d.data.median_salary)
          .style("left", d3.event.pageX + "px")
          .style("top", d3.event.pageY - 28 + "px");
      })
      .on("mouseout", function (d, i) {
        d3.select(this).transition().duration("50").attr("opacity", "1");
        div.transition().duration(200).style("opacity", 0);
      });

    var legend = svg
      .selectAll(".legend")
      .data(data)
      .enter()
      .append("g")
      .attr("class", "legend")
      .attr("transform", "translate(" + 850 + "," + 120 + ")");

    legend
      .append("rect")
      .attr("x", 0)
      .attr("y", function (d, i) {
        return 20 * i;
      })
      .attr("width", 15)
      .attr("height", 15)
      .style("fill", function (d) {
        return color(d.name);
      });

    legend
      .append("text")
      .attr("x", 25)
      .attr("text-anchor", "start")
      .attr("dy", "1em")
      .attr("y", function (d, i) {
        return 20 * i;
      })
      .text(function (d) {
        return d.name;
      })
      .attr("font-size", "12px");

    legend
      .append("text")
      .attr("x", 31)
      .attr("dy", "-.2em")
      .attr("y", -10)
      .text("Call Type")
      .attr("font-size", "17px");
  }

  render() {
    return (
      <svg
        width={1300}
        height={1100}
        font-family={"sans-serif"}
        font-size={10}
        text-anchor={"middle"}
      ></svg>
    );
  }
}

export default JobsBubbleChart;
